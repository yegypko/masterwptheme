$pronrage = new 	/*
					 * The WordPress Query class.
					 *
					 * @link http://codex.wordpress.org/Function_Reference/WP_Query
					 */
					$args = array(
						
						// Post & Page Parameters
						'p'            => 1,
						'name'         => 'hello-world',
						'page_id'      => 1,
						'pagename'     => 'sample-page',
						'post_parent'  => 1,
						'post__in'     => array( 1, 2, 3 ),
						'post__not_in' => array( 1, 2, 3 ),
				
						// Author Parameters
						'author'      => '1,2,3',
						'author_name' => 'admin',
				
						// Category Parameters
						'cat'              => 1,
						'category_name'    => 'blog',
						'category__and'    => array( 1, 2 ),
						'category__in'     => array( 1, 2 ),
						'category__not_in' => array( 1, 2 ),
				
						// Type & Status Parameters
						'post_type'   => 'any',
						'post_status' => 'any',
				
						// Choose ^ 'any' or from below, since 'any' cannot be in an array
						'post_type' => array(
							'post',
							'page',
							'revision',
							'attachment',
							'my-post-type',
						),
				
						'post_status' => array(
							'publish',
							'pending',
							'draft',
							'auto-draft',
							'future',
							'private',
							'inherit',
							'trash',
						),
				
						// Order & Orderby Parameters
						'order'               => 'DESC',
						'orderby'             => 'date',
						'ignore_sticky_posts' => false,
						'year'                => 2012,
						'monthnum'            => 1,
						'w'                   => 1,
						'day'                 => 1,
						'hour'                => 12,
						'minute'              => 5,
						'second'              => 30,
				
						// Tag Parameters
						'tag'           => 'cooking',
						'tag_id'        => 5,
						'tag__and'      => array( 1, 2 ),
						'tag__in'       => array( 1, 2 ),
						'tag__not_in'   => array( 1, 2 ),
						'tag_slug__and' => array( 'red', 'blue' ),
						'tag_slug__in'  => array( 'red', 'blue' ),
				
						// Pagination Parameters
						'posts_per_page'         => 10,
						'posts_per_archive_page' => 10,
						'nopaging'               => false,
						'paged'                  => get_query_var( 'paged' ),
						'offset'                 => 3,
				
						// Custom Field Parameters
						'meta_key'       => 'key',
						'meta_value'     => 'value',
						'meta_value_num' => 10,
						'meta_compare'   => '=',
						'meta_query'     => array(
							array(
								'key'     => 'color',
								'value'   => 'blue',
								'type'    => 'CHAR',
								'compare' => '=',
							),
							array(
								'key'     => 'price',
								'value'   => array( 1,200 ),
								'compare' => 'NOT LIKE',
							),
						),
				
						// Taxonomy Parameters
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy'         => 'color',
								'field'            => 'slug',
								'terms'            => array( 'red', 'blue' ),
								'include_children' => true,
								'operator'         => 'IN',
							),
							array(
								'taxonomy'         => 'actor',
								'field'            => 'id',
								'terms'            => array( 1, 2, 3 ),
								'include_children' => false,
								'operator'         => 'NOT IN',
							)
						),
				
						// Permission Parameters -
						'perm' => 'readable',
				
						// Parameters relating to caching
						'no_found_rows'          => false,
						'cache_results'          => true,
						'update_post_term_cache' => true,
						'update_post_meta_cache' => true,
				
					);
				
				$query = new WP_Query( $args );












function master_wp_theme_post_types(){
	


	register_post_type( 'slider', array(
		'public' => true,
		
		'labels' => array(
		 'name'  => 'slider'
		)
	));

}

add_action( 'init', 'master_wp_theme_post_types' );



/**
	 * Registers a new post type
	 * @uses $wp_post_types Inserts new post type object into the list
	 *
	 * @param string  Post type key, must not exceed 20 characters
	 * @param array|string  See optional args description above.
	 * @return object|WP_Error the registered post type object, or an error object
	 */

	
	function master_wp_theme_post_types() {
	
		$labels = array(
			'name'               => __( 'Slider', 'slider' ),
			'singular_name'      => __( 'Slider', 'slider' ),
			'add_new'            => _x( 'Add New Slider', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Slider', 'slider' ),
			'edit_item'          => __( 'Edit Slider', 'slider' ),
			'new_item'           => __( 'New Slider', 'slider' ),
			'view_item'          => __( 'View Slider', 'slider' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Slider', 'slider' ),
			'all_items'			=> 'All Sliders',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => true,
			// 'show_ui'             => true,
			// 'show_in_menu'        => true,
			// 'show_in_admin_bar'   => true,
			// 'menu_position'       => null,
			'menu_icon'           => 'dashicons-image-flip-horizontal',
			// 'show_in_nav_menus'   => true,
			// 'publicly_queryable'  => true,
			// 'exclude_from_search' => false,
			// 'has_archive'         => true,
			// 'query_var'           => true,
			// 'can_export'          => true,
			// 'rewrite'             => array( 'slug' => 'slider' ),
			// 'capability_type'     => 'post',
			'supports'            => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'custom-fields',
				'trackbacks',
				'comments',
				'revisions',
				'page-attributes',
				'post-formats',
			)
			
		);
	
		register_post_type( 'slider', $args );
	};
	
	add_action( 'init', 'master_wp_theme_post_types' );







	You should look at register_post_type parameters. You should probably set them like this:

'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
'publicly_queryable' => true,  // you should be able to query it
'show_ui' => true,  // you should be able to edit it in wp-admin
'exclude_from_search' => true,  // you should exclude it from search results
'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
'has_archive' => false,  // it shouldn't have archive page
'rewrite' => false,  // it shouldn't have rewrite rules



<?php 
					$args = array(
						'posts_per_pages' => '2',
						'post_type'   => 'post',
					);
				
				$frontpageSlider = new WP_Query( $args);
				while ( $frontpageSlider->have_posts()) {
					$frontpageSlider->the_post(); ?>
					<li><?php the_title();?></li>
				<?php }
				
			?>

			<?php 
				$frontpageSlider = new WP_Query (array(
						'posts_per_pages' => '2',
						'post_type'   => 'slider',
					));
				
				while ( $frontpageSlider->have_posts()) {
					$frontpageSlider->the_post(); ?>
					<li><?php the_title();?></li>
				<?php }
				
			?>


			<?php 


				


			?>


			<?php
					if (is_category()) {
						echo single_cat_title();
					}
				?>
				<!-- вывести категорию вп квери, ее ту просто нет
				Категория есть в квери, она работает, но вывести я ее могу только на стандартных вордпресс страницах типа архива, т.е. ни одна стандартная функция для работы с категориями не работает на кастомной странице. Я могу только создать кастомное поле с взаимосвязью и такии образом вывести без привязки к тайтлу. + надо проверить правильно ли у меня выведены таксономи категории в кастом пост тайп и вообще как они так получились)))
				 -->
				https://crunchify.com/how-to-create-wordpress-custom-post-type-cpt-and-taxonomy-hello-world-tutorial-tips-and-tricks/

				https://code.tutsplus.com/tutorials/wordpress-custom-post-types-taxonomies-admin-columns-filters--wp-27898

				https://developer.mozilla.org/ru/docs/Web/Guide/HTML/Using_data_attributes

				https://docs.buddyforms.com/article/153-create-a-new-post-type-and-taxonomy-with-the-cpt-ui-plugin

				https://code.tutsplus.com/tutorials/wp_query-arguments-categories-and-tags--cms-23070

				функции вывода категория
				https://misha.blog/wordpress/single_cat_title.html
				https://misha.blog/wordpress/is_category.html

				wp_query
				https://wp-kama.ru/function/wp_query#parametry-taksonomij
				



				/**
		 * Create a taxonomy
		 *
		 * @uses  Inserts new taxonomy object into the list
		 * @uses  Adds query vars
		 *
		 * @param string  Name of taxonomy object
		 * @param array|string  Name of the object type for the taxonomy object.
		 * @param array|string  Taxonomy arguments
		 * @return null|WP_Error WP_Error if errors, otherwise null.
		 */
		function my_taxonomies_name() {
		
			$labels = array(
				'name'                  => _x( 'Plural Name', 'Taxonomy plural name', 'text-domain' ),
				'singular_name'         => _x( 'Singular Name', 'Taxonomy singular name', 'text-domain' ),
				'search_items'          => __( 'Search Plural Name', 'text-domain' ),
				'popular_items'         => __( 'Popular Plural Name', 'text-domain' ),
				'all_items'             => __( 'All Plural Name', 'text-domain' ),
				'parent_item'           => __( 'Parent Singular Name', 'text-domain' ),
				'parent_item_colon'     => __( 'Parent Singular Name', 'text-domain' ),
				'edit_item'             => __( 'Edit Singular Name', 'text-domain' ),
				'update_item'           => __( 'Update Singular Name', 'text-domain' ),
				'add_new_item'          => __( 'Add New Singular Name', 'text-domain' ),
				'new_item_name'         => __( 'New Singular Name Name', 'text-domain' ),
				'add_or_remove_items'   => __( 'Add or remove Plural Name', 'text-domain' ),
				'choose_from_most_used' => __( 'Choose from most used Plural Name', 'text-domain' ),
				'menu_name'             => __( 'Singular Name', 'text-domain' ),
			);
		
			$args = array(
				'labels'            => $labels,
				'public'            => true,
				'show_in_nav_menus' => true,
				'show_admin_column' => false,
				'hierarchical'      => false,
				'show_tagcloud'     => true,
				'show_ui'           => true,
				'query_var'         => true,
				'rewrite'           => true,
				'query_var'         => true,
				'capabilities'      => array(),
			);
		
			register_taxonomy( 'taxonomy-slug', array( 'post' ), $args );
		}
		
		add_action( 'init', 'my_taxonomies_name' );


		https://wp-kama.ru/function/register_post_type
		https://wp-kama.ru/function/register_taxonomy
		https://wp-kama.ru/function/WP_Taxonomy
		https://developer.wordpress.org/reference/classes/wp_taxonomy/




		 <article <?php comment_class(); ?> class="comment">
 
	            <div class="comment-body">
		            <div class="author vcard">
			            <?php echo get_avatar( $comment, 100 ); ?>
			            	<span class="author-name"><?php comment_author(); ?></span>
			            <?php comment_text(); ?>
		            </div><!-- .vcard -->
	            </div><!-- comment-body -->


	            
	 
	            <footer class="comment-footer">
		            <time <?php comment_time( 'c' ); ?> class="comment-time">
			            <span class="date"><?php comment_date(); ?></span>
			            <span class="time"><?php comment_time(); ?></span>
		            </time>
		            <div class="reply"><?php comment_reply_link( array_merge( $args, array( 
								            'reply_text' => 'Reply',
								            'after' => ' <span>&amp;amp;darr;</span>', 
								            'depth' => $depth,
								            'max_depth' => $args['max_depth'] 
		            						) ) ); ?>
		            </div><!-- .reply -->
	            </footer><!-- .comment-footer -->
	 
	        </article><!-- #comment-<?php comment_ID(); ?> -->


	     function custom_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' : ?>
            <li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
            <div class="back-link">< ?php comment_author_link(); ?></div>
        <?php break;
        default : ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <article <?php comment_class(); ?> class="comment">
 
            <div class="comment-body">
            <div class="author vcard">
            <?php echo get_avatar( $comment, 100 ); ?>
            <span class="author-name"><?php comment_author(); ?></span>
            <?php comment_text(); ?>
            </div><!-- .vcard -->
            </div><!-- comment-body -->
 
            <footer class="comment-footer">
            <time <?php comment_time( 'c' ); ?> class="comment-time">
            <span class="date">
            <?php comment_date(); ?>
            </span>
            <span class="time">
            <?php comment_time(); ?>
            </span>
            </time>
            <div class="reply"><?php 
            comment_reply_link( array_merge( $args, array( 
            'reply_text' => 'Reply',
            'after' => ' <span>&amp;amp;darr;</span>', 
            'depth' => $depth,
            'max_depth' => $args['max_depth'] 
            ) ) ); ?>
            </div><!-- .reply -->
            </footer><!-- .comment-footer -->
 
            </article><!-- #comment-<?php comment_ID(); ?> -->
        <?php // End the default styling of comment
        break;
    endswitch;
}



https://wp-kama.ru/function/comment_form
https://blog.josemcastaneda.com/2013/05/29/custom-comment/











// В functions.php подключаем rest api + nonce

wp_localize_script('master_wp_theme-js', 'masterWpThemeData', array(
    				'root_url' => get_site_url(),
    				'nonce' => wp_create_nonce('wp_rest')
  	));

// переменные: master_wp_theme-js (но название как и у основной функции js)
// 		masterWpThemeData - название базы 

//---------------------------------------------------------------------------


// В functions.php либо дочернем файле для постов создаем register_post_type

function master_wp_theme_post_types() {
		// Like
		$labels = array(
			'name'               => __( 'Like', 'like' ),
			'singular_name'      => __( 'Like', 'like' ),
			'add_new'            => _x( 'Add New Like', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Like', 'like' ),
			'edit_item'          => __( 'Edit Like', 'like' ),
			'new_item'           => __( 'New Like', 'like' ),
			'view_item'          => __( 'View Like', 'like' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Like', 'like' ),
			'all_items'			=> 'All Likes',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-heart',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			// 'capability_type'     => 'muve', //for user role
			// 'map_meta_cap'		  => true //for user role
			// 'show_in_rest'       => true,
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'like', $args );

//переменные: 
		// like, но можно так и оставить

//--------------------------------------------------------------------

//в файле single-... добавляем визуальную часть (front-end)


				            $likeCount = new WP_Query(array(
				                'post_type' => 'like',
				                'meta_query' => array(
				                  array(
				                    'key' => 'liked_post_id',
				                    'compare' => '=',
				                    'value' => get_the_ID()
				                  )
				                )
				              ));

				              $existStatus = 'no';

					              if (is_user_logged_in()) {
					                $existQuery = new WP_Query(array(
					                  'author' => get_current_user_id(),
					                  'post_type' => 'like',
					                  'meta_query' => array(
					                    array(
					                      'key' => 'liked_post_id',
					                      'compare' => '=',
					                      'value' => get_the_ID()
					                    )
					                  )
					                ));

					                if ($existQuery->found_posts) {
					                  $existStatus = 'yes';
					                }
					              }
//переменные: 	like (ранее созданный пост)	
		// 	liked_post_id (custom field созданный в админке)

		?>
		<a href="#" class="like-box" data-like="<?php echo $existQuery->posts[0]->ID; ?>" data-post="<?php the_ID(); ?>" data-exists="<?php echo $existStatus; ?>">
			<i class="fa fa-heart" aria-hidden="true"></i> 
			<span class="like-count"><?php echo $likeCount->found_posts;?></span>
		</a>
		<?php	   
//переменные :  data-like, data-post, data-exists (но их можно писать так же, в целом не считается кроме post - это название страницы на которой выводится лайк, в целом можно придумать унифицированное, подходящее для любой страницы или поста) 

//-------------------------------------------------------------------


// В functions.php либо дочернем файле

add_action('rest_api_init', 'masterWpLikeRoutes');

function masterWpLikeRoutes() {
  register_rest_route('masterwp/v1', 'manageLike', array(
    'methods' => 'POST',
    'callback' => 'createLike'
  ));

  register_rest_route('masterwp/v1', 'manageLike', array(
    'methods' => 'DELETE',
    'callback' => 'deleteLike'
  ));
}


function createLike($data) {
	if (is_user_logged_in()) {
		$postLike = sanitize_text_field($data['postId']);
		$existQuery = new WP_Query(array(
					                  'author' => get_current_user_id(),
					                  'post_type' => 'like',
					                  'meta_query' => array(
					                    array(
					                      'key' => 'liked_post_id',
					                      'compare' => '=',
					                      'value' => $postLike
					                    )
					                  )
					                ));
		if ($existQuery->found_posts == 0 AND get_post_type($postLike) == 'post') {
			return wp_insert_post(
			array(
	        'post_type' => 'like',
	        'post_status' => 'publish',
	        'post_title' => 'Created Likes',
	        'meta_input' => array(
	        	'liked_post_id' => $postLike
	        )
			)
		);
		} else {
			die("Invalid post id");
		}
		
		
	} else {
		die("Only logged in users can create a like.");
	}
	
	
}

function deleteLike($data) {
  $likeId = sanitize_text_field($data['like']);
  if (get_current_user_id() == get_post_field('post_author', $likeId) AND get_post_type($likeId) == 'like') {
    wp_delete_post($likeId, true);
    return 'Congrats, like deleted.';
  } else {
    die("You do not have permission to delete that.");
  }
}

//переменные: 'post_type' => 'like' (все тот же like который был выше)
   				//'key' => 'liked_post_id' (все тот же custom field)
   				// 'post_title' => 'Created Likes', (можно менять название в появившемся посте с лайками в админке, а можно и оставить)
				//masterwp/v1 (произвольное, но можно унифицировать)
				//manageLike (произвольное, но можно унифицировать)
				

//---------------------------------------------------------------------------------------------------------------------------------

//В файле JS либо дочернем файле соответствующий теме пишем REST (записываем и удаляем из базы данных)

constructor();

  function constructor() {
   events();
  }
  
  //events
  function events() {
    $(".like-box").on("click", this.ourClickDispatcher.bind(this));
  }

  // methods
  function ourClickDispatcher(e) {
    var currentLikeBox = $(e.target).closest(".like-box");
    // currentLikeBox.attr('data-exists') == 'yes'

    if (currentLikeBox.attr('data-exists') == 'yes') {
      this.deleteLike(currentLikeBox);
    } else {
      this.createLike(currentLikeBox);
    }
  }

  function createLike(currentLikeBox) {
    
    $.ajax({
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-WP-Nonce', masterWpThemeData.nonce);
      },
      url: masterWpThemeData.root_url + '/wp-json/masterwp/v1/manageLike',
      type: 'POST',
      data: {'postId': currentLikeBox.data('post')},
      success: (response) => {
        currentLikeBox.attr('data-exists', 'yes');
        var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount++;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", response);
        console.log(response);
      },
      error: (response) => {
        console.log(response);
      }
    });

  }

  function deleteLike(currentLikeBox) {
    $.ajax({
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-WP-Nonce', masterWpThemeData.nonce);
      },
     url: masterWpThemeData.root_url + '/wp-json/masterwp/v1/manageLike',
      type: 'DELETE',
      data: {'like': currentLikeBox.attr('data-like')},
      success: (response) => {
         currentLikeBox.attr('data-exists', 'no');
        var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount--;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", '');
        console.log(response);
      },
      error: (response) => {
        console.log(response);
      }
    });
  }

  //Переменные: masterWpThemeData (назавние базы, которое мы прописали в файле function.php)
  				//masterwp/v1/manageLike (было сформировано в function.php)
  				// data-like, like-count(класс), data-exists, like-box(класс) post (это data-post) - это все переменные которые можно унифицировать и не менять




