<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'master_wp_theme');

/** MySQL database username */
define('DB_USER', 'user_master');

/** MySQL database password */
define('DB_PASSWORD', 'clZLTa6wzinVGOj1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7X(z#~ 6?aqt;9W&;W/)D[*S+8;*9j4m54ZjfD}c?1BCxQ_Qr/]k7Cy Km LnU%N');
define('SECURE_AUTH_KEY',  'wz+cOnGZ?kt.5+C<#$:NA@>A%wg-)2}G&~!mnymw{&dmNT)_L .JCPK$-QNK8*wF');
define('LOGGED_IN_KEY',    'S;>?-{C-B=Dr8 4Z;%h*9d+T_hb*[]6QT5d&7UV#K9z4klDK&!7%|0!6D!]pzjQh');
define('NONCE_KEY',        '5Z*iTXf0D)V3zuNg@#<WCWt20hT%S4b0Z<c+<oY(0o+m =B>2I^oELfF1=zyZiW%');
define('AUTH_SALT',        'N9jje.f`ISZ8&vb>:`UBfOORx?FjW@a|W6L49||I+=8p0*wF~#{{n,JL(oV%L]1w');
define('SECURE_AUTH_SALT', 'G[n> =8V<IoUSZ98-cybxQ3S/NU--&&U0-<~)|V&ANh!`G,d|h}*?oN!%*g!:,HJ');
define('LOGGED_IN_SALT',   'gq9(=.Bb>M>2#_9TeXTxGRH}vuH%p3vY_]Gwzr-}D&#2fU?~`-|Hc<14Om.%Hr&1');
define('NONCE_SALT',       'IFhA ;(@r#GmFQAvI2N)Ch#z1|.^h2|*&)S.W7J)aH^MD|{s!WXGstR! Q6)/&b|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dbms_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
