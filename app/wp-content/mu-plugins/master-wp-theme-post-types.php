<?php
/**
	 * Registers a new post type
	 * @uses $wp_post_types Inserts new post type object into the list
	 *
	 * @param string  Post type key, must not exceed 20 characters
	 * @param array|string  See optional args description above.
	 * @return object|WP_Error the registered post type object, or an error object
	 */

	
	function master_wp_theme_post_types() {
		// Like
		$labels = array(
			'name'               => __( 'Like', 'like' ),
			'singular_name'      => __( 'Like', 'like' ),
			'add_new'            => _x( 'Add New Like', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Like', 'like' ),
			'edit_item'          => __( 'Edit Like', 'like' ),
			'new_item'           => __( 'New Like', 'like' ),
			'view_item'          => __( 'View Like', 'like' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Like', 'like' ),
			'all_items'			=> 'All Likes',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-heart',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			// 'capability_type'     => 'muve', //for user role
			// 'map_meta_cap'		  => true //for user role
			// 'show_in_rest'       => true,
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'like', $args );

		// Post Slider
		$labels = array(
			'name'               => __( 'Slider', 'slider' ),
			'singular_name'      => __( 'Slider', 'slider' ),
			'add_new'            => _x( 'Add New Slider', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Slider', 'slider' ),
			'edit_item'          => __( 'Edit Slider', 'slider' ),
			'new_item'           => __( 'New Slider', 'slider' ),
			'view_item'          => __( 'View Slider', 'slider' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Slider', 'slider' ),
			'all_items'			=> 'All Sliders',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-image-flip-horizontal',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				// 'editor',
				'author',
				'thumbnail',
				'excerpt',
				
			)
			
		);
	
		register_post_type( 'slider', $args );

		// Post Services
		$labels = array(
			'name'               => __( 'Services', 'services' ),
			'singular_name'      => __( 'Services', 'services' ),
			'add_new'            => _x( 'Add New Services', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Services', 'services' ),
			'edit_item'          => __( 'Edit Services', 'services' ),
			'new_item'           => __( 'New Services', 'services' ),
			'view_item'          => __( 'View Services', 'services' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Services', 'services' ),
			'all_items'			=> 'All Services',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array(),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-smiley',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'services', $args );

		// Features Services
		$labels = array(
			'name'               => __( 'Features', 'features' ),
			'singular_name'      => __( 'Features', 'features' ),
			'add_new'            => _x( 'Add New Features', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Features', 'features' ),
			'edit_item'          => __( 'Edit Features', 'features' ),
			'new_item'           => __( 'New Features', 'features' ),
			'view_item'          => __( 'View Features', 'features' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Features', 'features' ),
			'all_items'			=> 'All Features',
		);
	
		$args = array(
			'labels'              => $labels,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-phone',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				
			)
			
		);
	
		register_post_type( 'features', $args );

		// Portfolio Slider
		$labels = array(
			'name'               => __( 'Portfolio', 'portfolio' ),
			'singular_name'      => __( 'Portfolio', 'portfolio' ),
			'add_new'            => _x( 'Add New Portfolio', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Portfolio', 'portfolio' ),
			'edit_item'          => __( 'Edit Portfolio', 'portfolio' ),
			'new_item'           => __( 'New Portfolio', 'portfolio' ),
			'view_item'          => __( 'View Portfolio', 'portfolio' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Portfolio', 'portfolio' ),
			'all_items'			=> 'All Portfolio',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('Portfolio-category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-format-gallery',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'portfolio', $args );



		// Choose
		$labels = array(
			'name'               => __( 'Choose', 'choose' ),
			'singular_name'      => __( 'Choose', 'choose' ),
			'add_new'            => _x( 'Add New Choose', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Choose', 'choose' ),
			'edit_item'          => __( 'Edit Choose', 'choose' ),
			'new_item'           => __( 'New Choose', 'choose' ),
			'view_item'          => __( 'View Choose', 'choose' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Choose', 'choose' ),
			'all_items'			=> 'All Choose',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-thumbs-up',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				
			)
			
		);
	
		register_post_type( 'choose', $args );

		// Work
		$labels = array(
			'name'               => __( 'Work', 'work' ),
			'singular_name'      => __( 'Work', 'work' ),
			'add_new'            => _x( 'Add New Work', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Work', 'work' ),
			'edit_item'          => __( 'Edit Work', 'work' ),
			'new_item'           => __( 'New Work', 'work' ),
			'view_item'          => __( 'View Work', 'work' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Work', 'work' ),
			'all_items'			=> 'All Work',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-hammer',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				
			)
			
		);
	
		register_post_type( 'work', $args );

		// Apllication
		$labels = array(
			'name'               => __( 'Apllication', 'apllication' ),
			'singular_name'      => __( 'Apllication', 'apllication' ),
			'add_new'            => _x( 'Add New Apllication', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Apllication', 'apllication' ),
			'edit_item'          => __( 'Edit Apllication', 'apllication' ),
			'new_item'           => __( 'New Apllication', 'apllication' ),
			'view_item'          => __( 'View Apllication', 'apllication' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Apllication', 'apllication' ),
			'all_items'			=> 'All Apllication',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-lightbulb',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				'editor',
				// 'author',
				'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'apllication', $args );

		// Genius
		$labels = array(
			'name'               => __( 'Genius', 'genius' ),
			'singular_name'      => __( 'Genius', 'genius' ),
			'add_new'            => _x( 'Add New Genius', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Genius', 'genius' ),
			'edit_item'          => __( 'Edit Genius', 'genius' ),
			'new_item'           => __( 'New Genius', 'genius' ),
			'view_item'          => __( 'View Genius', 'genius' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Genius', 'genius' ),
			'all_items'			=> 'All Genius',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-art',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'genius', $args );


		// Muve
		$labels = array(
			'name'               => __( 'Muve', 'muve' ),
			'singular_name'      => __( 'Muve', 'muve' ),
			'add_new'            => _x( 'Add New Muve', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Muve', 'muve' ),
			'edit_item'          => __( 'Edit Muve', 'muve' ),
			'new_item'           => __( 'New Muve', 'muve' ),
			'view_item'          => __( 'View Muve', 'muve' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Muve', 'muve' ),
			'all_items'			=> 'All Muve',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-video-alt3',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'muve', //for user role
			// 'map_meta_cap'		  => true //for user role
			// 'show_in_rest'       => true,
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'muve', $args );

		// Contact
		$labels = array(
			'name'               => __( 'Contact', 'contact' ),
			'singular_name'      => __( 'Contact', 'contact' ),
			'add_new'            => _x( 'Add New Contact', 'text-domain', 'text-domain' ),
			'add_new_item'       => __( 'Add New Contact', 'contact' ),
			'edit_item'          => __( 'Edit Contact', 'contact' ),
			'new_item'           => __( 'New Contact', 'contact' ),
			'view_item'          => __( 'View Contact', 'contact' ),
			'search_items'       => __( 'Search Plural Name', 'text-domain' ),
			'not_found'          => __( 'No Plural Name found', 'text-domain' ),
			'not_found_in_trash' => __( 'No Plural Name found in Trash', 'text-domain' ),
			'parent_item_colon'  => __( 'Parent Singular Name:', 'text-domain' ),
			'menu_name'          => __( 'Contact', 'contact' ),
			'all_items'			=> 'All Contact',
		);
	
		$args = array(
			'labels'              => $labels,
			// 'hierarchical'        => false,
			// 'description'         => 'description',
			// 'taxonomies'          => array('category'),
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-googleplus',
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => false,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				// 'editor',
				// 'author',
				// 'thumbnail',
				// 'excerpt',
				
			)
			
		);
	
		register_post_type( 'contact', $args );
		
	};
	
	add_action( 'init', 'master_wp_theme_post_types' );

function portfolio_category() {
		
			$labels = array(
				'name'                  => _x( 'Portfolio Category', 'Portfolio Category', 'portfolio-category' ),
				'singular_name'         => _x( 'Portfolio Category', 'Portfolio Category', 'portfolio-category' ),
				'search_items'          => __( 'Search Portfolio Category', 'portfolio-category' ),
				'popular_items'         => __( 'Popular Portfolio Category', 'portfolio-category' ),
				'all_items'             => __( 'All PPortfolio Category', 'portfolio-category' ),
				'parent_item'           => __( 'Parent Portfolio Category', 'portfolio-category' ),
				'parent_item_colon'     => __( 'Parent Portfolio Category', 'portfolio-category' ),
				'edit_item'             => __( 'Edit Portfolio Category', 'portfolio-category' ),
				'update_item'           => __( 'Update Portfolio Category', 'portfolio-category' ),
				'add_new_item'          => __( 'Add New SPortfolio Category', 'portfolio-category' ),
				'new_item_name'         => __( 'New Portfolio Category', 'portfolio-category' ),
				'add_or_remove_items'   => __( 'Add or remove Portfolio Category', 'portfolio-category' ),
				'choose_from_most_used' => __( 'Choose from most used Portfolio Category', 'portfolio-category' ),
				'menu_name'             => __( 'Portfolio Category', 'portfolio-category' ),
			);
		
			$args = array(
				'labels'            => $labels,
				'public'            => true,
				'show_in_nav_menus' => true,
				'show_admin_column' => false,
				'hierarchical'      => true,
				'show_tagcloud'     => true,
				'show_ui'           => true,
				'query_var'         => true,
				'rewrite'           => true,
				'query_var'         => true,
				'capabilities'      => array(),
			);
		
			register_taxonomy( 'Portfolio-category', array( 'portfolio' ), $args );
		}
		
		add_action( 'init', 'portfolio_category' );