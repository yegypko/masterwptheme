<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo get_theme_file_uri('/images/favicon.png')?>" rel="icon"/>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5/html5shiv.min.js"></script>
      <script src="js/html5/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
	<!-- Custom style Theme Options -->
	<style>
		.service-box-inner:hover .icon-srv, .header-section, .feature-box-inner:hover .icon-feature,
		.portfolio-categories > li > a:hover, .portfolio-categories > li > a.active, .feature-box-inner:hover .line > i, .how-we-work ul li:hover a > i, .we-perfection-box-inner, .clean-code-box-inner, .we-launch-box-inner, .progress-bar-danger, .statistics-section, .icon i, .dates, .read-more:focus, .read-more:hover{
		    background-color: <?php the_field('color', 'option'); ?>;
		}
		.features-section:before, .why-choose:before, .we-perfection-slope:before, .clean-code:before, .we-launch-slope:before{
			border-top-color: <?php the_field('color', 'option'); ?>;
			border-bottom-color: <?php the_field('color', 'option'); ?>;
			opacity: 0.9;
		}
		.feature-box-inner .icon-feature, .how-we-work ul li a i, .app-updates h4 i{
			color: <?php the_field('color', 'option'); ?>;
		}
		.portfolio-block-hover, .hover{
			background-color: <?php the_field('color', 'option'); ?>;
			opacity: 0.9;
		}
		.how-we-work ul li a i, .how-we-work ul li:before{
			border-color: <?php the_field('color', 'option'); ?>
		}
		.research-slope:before{
			border-bottom-color: <?php the_field('color', 'option'); ?>;
			opacity: 0.9;
		}
		.project-section:before{
			border-bottom-color: <?php the_field('color', 'option'); ?>;
			border-top-color: <?php the_field('color', 'option'); ?>;
		}
		@media only screen and (min-width: 1367px) and (max-width: 1550px){
			.we-perfection-slope::before, .clean-code::before{
				background-color: <?php the_field('color', 'option'); ?>;
			}
		}
		}
	</style>
	<!-- Custom style Theme Options END-->
</head>
<body <?php body_class(); ?> data-offset="200" data-spy="scroll" data-target=".primary-navigation">

<!-- Own section -->
	
	<!-- Header Section -->
	<header id="header-section" class="header-section ">
		<div class="col-md-2 col-sm-2 col-xs-6 logo-block">
			<a href="<?php echo site_url()?>"><img src="<?php $LogoImage = get_field('logo', 'option'); echo $LogoImage['sizes']['ImageLogoHeder']?>" alt="logo"/></a>
		</div>
		<div class="col-md-10 col-sm-12 col-xs-12 menu-block">
			<nav class="navbar navbar-default primary-navigation" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				
				<!-- <ul class="nav navbar-nav navbar-right"> -->
				        
      			<!-- </ul> -->
      			
				<div id="navbar" class="navbar-collapse collapse">

					<ul class="nav navbar-nav pull-right">

						<li><a href="<?php echo site_url('#top')?>">Home</a></li>
						<li><a href="<?php echo site_url('#service-section')?>">Services</a></li>
						<li><a href="<?php echo site_url('#our-work')?>">Work</a></li>
						<li><a href="<?php echo site_url('#our-genius')?>">Team</a></li>
						<li><a href="<?php echo site_url('#project-section')?>">Contact</a></li>
						<!-- <li><a href="<?php echo site_url('/blog')?>">Blog</a></li> -->
						<?php
							wp_nav_menu(array(
								'theme_location' => 'menu-1',
								'container' => 'navbar',
								'container_class' => 'navbar-collapse collapse',
								'menu_class' => 'nav navbar-nav pull-right'
							)); 
						?>
						<!-- <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
							<ul id="login-dp" class="dropdown-menu">
								<li>
									 <div class="row">
											<div class="col-md-12">
												Login via
												<div class="social-buttons">
													<a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
													<a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
												</div>
				                                or
												 <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
														<div class="form-group">
															 <label class="sr-only" for="exampleInputEmail2">Email address</label>
															 <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
														</div>
														<div class="form-group">
															 <label class="sr-only" for="exampleInputPassword2">Password</label>
															 <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
				                                             <div class="help-block text-right"><a href="">Forget the password ?</a></div>
														</div>
														<div class="form-group">
															 <button type="submit" class="btn btn-primary btn-block">Sign in</button>
														</div>
														<div class="checkbox">
															 <label>
															 <input type="checkbox"> keep me logged-in
															 </label>
														</div>
												 </form>
											</div>
											<div class="bottom text-center">
												New here ? <a href="#"><b>Join Us</b></a>
											</div>
									 </div>
								</li>
							</ul>
				        </li> -->
						<?php if(is_user_logged_in()) { ?>
						<li class="logout"> <a href="<?php echo wp_logout_url();?>" >logout<i class="fa fa-sign-out"></i></a></li>
						<?php } else { ?>
						<li class="login"> <a href="<?php echo wp_login_url();?>">login<i class="fa fa-sign-in"></i></a></li>
						<li class="Sign-up"> <a href="<?php echo wp_registration_url();?>">Sign Up<i class="fa fa-plus"></i></a></li>
				       	<?php } ?>
					</ul>
					
				</div><!--/.nav-collapse -->
			</nav>
		</div>
	</header>
	<!-- Header Section Over -->
