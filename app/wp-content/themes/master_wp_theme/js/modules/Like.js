
constructor();

  function constructor() {
   events();
  }
  
  //events
  function events() {
    $(".like-box").on("click", this.ourClickDispatcher.bind(this));
  }

  // methods
  function ourClickDispatcher(e) {
    var currentLikeBox = $(e.target).closest(".like-box");
    // currentLikeBox.attr('data-exists') == 'yes'

    if (currentLikeBox.attr('data-exists') == 'yes') {
      this.deleteLike(currentLikeBox);
    } else {
      this.createLike(currentLikeBox);
    }
  }

  function createLike(currentLikeBox) {
    
    $.ajax({
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-WP-Nonce', masterWpThemeData.nonce);
      },
      url: masterWpThemeData.root_url + '/wp-json/masterwp/v1/manageLike',
      type: 'POST',
      // data: {'postId': 987},
      data: {'postId': currentLikeBox.data('post')},
    //   success: (response) => {
    //     console.log(response);
    //   },
      success: (response) => {
        currentLikeBox.attr('data-exists', 'yes');
        var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount++;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", response);
        console.log(response);
      },
      error: (response) => {
        console.log(response);
      }
    });

  }

  function deleteLike(currentLikeBox) {
    $.ajax({
      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-WP-Nonce', masterWpThemeData.nonce);
      },
     url: masterWpThemeData.root_url + '/wp-json/masterwp/v1/manageLike',
      type: 'DELETE',
      data: {'like': currentLikeBox.attr('data-like')},
      success: (response) => {
         currentLikeBox.attr('data-exists', 'no');
        var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount--;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", '');
        console.log(response);
      },
      error: (response) => {
        console.log(response);
      }
    });
  }




