<?php get_header(); ?>

<a id="top"></a>
	
	<!-- LOADER -->
	<!-- <div id="site-loader" class="load-complete">
		<div class="load-position">
			<div class="logo"><img src="<?php echo get_theme_file_uri('/images/loading-logo.png')?>" alt="loading-logo"/></div>
			<h6>Please wait, loading...</h6>
			<div class="loading">
				<div class="loading-line"></div>
				<div class="loading-break loading-dot-1"></div>
				<div class="loading-break loading-dot-2"></div>
				<div class="loading-break loading-dot-3"></div>
			</div>
		</div>
	</div> -->
	
	<!-- Style Switcher -->
	<!-- <div class="color-switcher" id="choose_color">
		<a href="#" class="picker_close">
			<i class="fa fa-gear fa-spin" ></i>
		</a>
		<div class="theme-colours">
			<p>Choose Colour style</p>
			<ul>
				<li><a href="#." class="blue" id="default"></a></li>
				<li><a href="#." class="green" id="green"></a></li>
				<li><a href="#." class="red" id="red"></a></li>
				<li><a href="#." class="yellow" id="yellow"></a></li>
				<li><a href="#." class="light-green" id="light-green"></a></li>
				<li><a href="#." class="orange" id="orange"></a></li>
				<li><a href="#." class="pink" id="pink"></a></li>
				<li><a href="#." class="black" id="black"></a></li>
			</ul>
		</div>
	</div> -->
	
	<!-- Slider Section -->
	<div id="photos-slider" class="slider-section">
		<div class="slides-container">
			<!-- slide item  -->
			<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'slider',
					);
				
				$frontpageSlider = new WP_Query( $args);
				while ( $frontpageSlider->have_posts()) {
					$frontpageSlider->the_post('slider'); ?>
					<div class="slide-item">
				<?php the_post_thumbnail('sliderImage');?>
				<h2 class="slide-title">
					<span class="inner-circle">
						<span><?php the_title();?></span>
					</span>
				</h2>
				<div class="slider-content">
					<div class="col-md-6 left">
						<h3 class="slide-sub-title wow fadeInDown" data-wow-duration="1s" data-wow-delay="0s">
							<span><?php the_field('slider_content_left');?></span>
						</h3>
					</div>
					<div class="col-md-6 right">
						<h3 class="slide-sub-title wow fadeInDown" data-wow-duration="1s" data-wow-delay="0s">
						<span><?php the_field('slider_content_right');?></span>						
					</h3>
					</div>
				</div>				
			</div>
				<?php }
				wp_reset_postdata();
			?>
			<!-- slide item over -->
		</div>
		<nav class="slides-navigation">
			<a href="#" class="next"><i class="arrow_carrot-right"></i></a>
			<a href="#" class="prev"><i class="arrow_carrot-left"></i></a>
		</nav>
		<p class="goto-next"><?php the_field('section_bottom');?><a class="arrow animated ci bounce" href="#service-section"><i class="fa fa-angle-down"></i></a></p>
	</div>
	<!-- Slider Section Over-->

	<!-- Services Section -->
	<div id="service-section" class="service-section">
		<div class="container">
			<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'services',
					);
				
				$frontpageServices = new WP_Query( $args);
				while ( $frontpageServices->have_posts()) {
					$frontpageServices->the_post('services'); ?>

			<div class="service-bg">			
				<img src="<?php $ServicesImage = get_field('background_image'); echo $ServicesImage['sizes']['ServicesBackgroundImage']?>" alt="service-bg"/>
				<h3><?php the_title()?></h3> 
			</div> 

			<div class="col-md-6">
				<div class="service-box-inner">
					<div class="col-md-4 pull-right col-sm-5">
						<i class="icon-srv <?php the_field('serv_1_icon');?>"></i>
					</div>
					 <div class="col-md-8 content-box col-sm-7">
						<h3 class="block-title"><?php the_field('serv_1_title');?></h3>
						<p><?php the_field('serv_1_content');?></p>
					</div> 	
				</div>
			 </div>
			<div class="col-md-6">
				<div class="service-box-inner">
					<div class="col-md-4 col-sm-5">
						<i class="icon-srv <?php the_field('serv_2_icon');?>"></i>
					</div>
					<div class="col-md-8 content-box col-sm-7">
						<h3 class="block-title"><?php the_field('serv_2_title');?></h3>
						<p><?php the_field('serv_2_content');?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="service-box-inner">
					<div class="col-md-4 pull-right col-sm-5">
						<i class="icon-srv <?php the_field('serv_3_icon');?>"></i>
					</div>
					<div class="col-md-8 content-box col-sm-7">
						<h3 class="block-title"><?php the_field('serv_3_title');?></h3>
						<p><?php the_field('serv_3_content');?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="service-box-inner">
					<div class="col-md-4 col-sm-5">
						<i class="icon-srv <?php the_field('serv_4_icon');?>"></i>
					</div>
					<div class="col-md-8 content-box col-sm-7">
						<h3 class="block-title"><?php the_field('serv_4_title');?></h3>
						<p><?php the_field('serv_4_content');?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="service-box-inner">
					<div class="col-md-4 pull-right col-sm-5">
						<i class="icon-srv <?php the_field('serv_5_icon');?>"></i>
					</div>
					<div class="col-md-8 content-box col-sm-7">
						<h3 class="block-title"><?php the_field('serv_5_title');?></h3>
						<p><?php the_field('serv_5_content');?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="service-box-inner">
					<div class="col-md-4 col-sm-5">
						<i class="icon-srv <?php the_field('serv_6_icon');?>"></i>
					</div>
					<div class="col-md-8 content-box col-sm-7">
						<h3 class="block-title"><?php the_field('serv6_title');?></h3>
						<p><?php the_field('serv_6_content');?></p>
					</div>
				</div>
			</div>
			<?php }  wp_reset_postdata(); ?>
		</div>
	</div>
	<!-- Services Section Over -->
	
	<!-- Features Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'features',
					);
				
				$frontpageSFeatures = new WP_Query( $args);
				while ( $frontpageSFeatures->have_posts()) {
					$frontpageSFeatures->the_post('features'); ?>
	<section id="features-section" class="features-section ow-background-no-size" style="background-image: url(<?php $FeaturesBackgroundImage = get_field('features_background_image'); echo $FeaturesBackgroundImage['sizes']['FeaturesBackgroundImage'];?>);">
		<div class="container">
			<div class="col-md-7">
				<h2><?php the_title();?></h2>
				<div class="feature-box">
					<div class="feature-box-inner">
						<div class="col-md-4 col-sm-4 pull-right">
							<i class="icon-feature <?php the_field('feat_1_icon');?>"></i>
						</div>
						<div class="col-md-8 col-sm-8">
							<h3 class="block-title"><?php the_field('feat_1_title');?></h3>
							<p><?php the_field('feat_1_content');?></p>
						</div>						
						<div class="lightbulb line">
							<hr>
							<i></i>
						</div>
					</div>
					<div class="feature-box-inner">
						<div class="col-md-4 col-sm-4 pull-right">
							<i class="icon-feature <?php the_field('feat_2_icon');?>"></i>
						</div>
						<div class="col-md-8 col-sm-8">
							<h3 class="block-title"><?php the_field('feat_2_title');?></h3>
							<p><?php the_field('feat_2_content');?></p>
						</div>
						<div class="house line">
							<hr>
							<i></i>
						</div>
					</div>
					<div class="feature-box-inner">
						<div class="col-md-4 col-sm-4 pull-right">
							<i class="icon-feature <?php the_field('feat_3_icon');?>"></i>
						</div>
						<div class="col-md-8 col-sm-8">
							<h3 class="block-title"><?php the_field('feat_3_title');?></h3>
							<p><?php the_field('feat_3_content');?></p>
						</div>						
						<div class="gift line">
							<hr>
							<i></i>
						</div>
					</div>
					<div class="feature-box-inner">
						<div class="col-md-4 col-sm-4 pull-right">
							<i class="icon-feature <?php the_field('feat_4_icon');?>"></i>
						</div>
						<div class="col-md-8 col-sm-8">
							<h3 class="block-title"><?php the_field('feat_4_title');?></h3>
							<p><?php the_field('feat_4_content');?></p>
						</div>							
						<div class="camera line">
							<hr>
							<i></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="mobile-iphone">
					<?php $FeaturesImage = get_field('features_mobile_image'); ?>
					<img src="<?php echo $FeaturesImage['url']; ?>" alt="<?php echo $FeaturesImage['mobile']; ?>" />
				</div>
			</div>
		</div>
	</section>
	<?php }  wp_reset_postdata(); ?>
	<!-- Features Section Over -->
	
	<!-- Our Work -->
	<section id="our-work" class="our-work">
		<!-- Section Header -->
		<div class="section-header">
			<h2>OUR Work</h2>
		</div>
		<!-- Section Header Over -->
		
		<!-- Portfolio Gallery -->
		<div id="portfolio-gallery-no-space" class="portfolio-gallery">

			<ul class="portfolio-categories sorting-menu">
				<li data-value="all"><a class="active" href="#">Show ALL</a></li>
				<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'portfolio',
					);
				
					$frontpagePortfolio = new WP_Query( $args);
					while ( $frontpagePortfolio->have_posts()) {
						$frontpagePortfolio->the_post('portfolio'); ?>
				<li data-value="<?php echo the_field('category_relationship');?>"><a href="#"><?php echo the_title();?></a></li>
				<?php }  wp_reset_postdata(); ?>
			</ul><!--/.container -->
			<ul class="portfolio-list no-space"> 	
				<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'portfolio',
					);
					$frontpagePortfolio = new WP_Query( $args);
					while ( $frontpagePortfolio->have_posts()) {
						$frontpagePortfolio->the_post('portfolio');
							$images = get_field('portfolio_gallery');
							if( $images ): 
								foreach( $images as $image ): ?>
									<li class="col-md-3 col-sm-6" data-type="<?php the_field('category_relationship');?>" data-id="<?php the_field('category_relationship');?>">
										<div class="portfolio-image-block">
										            	<a href="<?php echo $image['url']; ?>">
									                     <img src="<?php echo $image['sizes']['PortfolioImage']; ?>" alt="<?php echo $image['Portfolio']; ?>" />
									                	</a>
											<div class="portfolio-block-hover">
												<a href="<?php $PortfolioBlockHover = get_field('portfolio_image_hover'); echo $PortfolioBlockHover['sizes']['PortfolioImage'];?>" class="portfolio-title" data-lightbox="portfolio" data-title="Image Title 1"><?php the_field('portfolio_description_hover');?></a>
												<h4><?php the_field('portfolio_title_hover');?></h4>
											</div>
										</div>
									</li>
								<?php endforeach; 
							endif; ?>
					<?php }  wp_reset_postdata(); ?>
			</ul>	
		</div>
	</section>
	<!-- Our Work Section Over -->
	
	<!-- Why Choose Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'choose',
					);
				
				$frontpageChoose = new WP_Query( $args);
				while ( $frontpageChoose->have_posts()) {
					$frontpageChoose->the_post('choose'); ?>

	<section id="why-choose" class="why-choose" style="background-image: url(<?php $ChooseBackgroundImage = get_field('choose_background_image'); echo $ChooseBackgroundImage['sizes']['FeaturesBackgroundImage'];?>)">
		<div class="container">			
			<div class="col-md-6 pull-right">
				<div class="why-choose-inner">
					<h2><?php the_title();?></h2>
					<div class="why-choose-box col-md-10 col-sm-8">
						<a href="#" class="block-title"><i class="icon-why <?php the_field('choose_icon_1');?>"></i><span><?php the_field('choose_title_1');?></span></a>
						<p><?php the_field('choose_content_1');?></p>
					</div>
					<div class="why-choose-box col-md-10 col-sm-8">
						<a href="#" class="block-title"><i class="icon-why <?php the_field('choose_icon_2');?>"></i><span><?php the_field('choose_title_2');?></span></a>
						<p><?php the_field('choose_content_2');?></p>
					</div>
					<div class="why-choose-box col-md-10 col-sm-8">
						<a href="#" class="block-title"><i class="icon-why <?php the_field('choose_icon_3');?>"></i><span><?php the_field('choose_title_3');?></span></a>
						<p><?php the_field('choose_content_3');?></p>
					</div>
					<div class="why-choose-box col-md-10 col-sm-8">
						<a href="#" class="block-title"><i class="icon-why <?php the_field('choose_icon_4');?>"></i><span><?php the_field('choose_title_4');?></span></a>
						<p><?php the_field('choose_content_4');?></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php }  wp_reset_postdata(); ?>
	<!-- Why Choose Section Over -->

	<!-- How We Work -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'work',
					);
				
				$frontpageWork = new WP_Query( $args);
				while ( $frontpageWork->have_posts()) {
					$frontpageWork->the_post('work'); ?>

	<section id="how-we-work" class="how-we-work">
		<!-- Section Header -->
		<div class="section-header">
			<h2><?php the_title()?></h2>
		</div>
		<!-- Section Header Over-->
		<div class="container">
			<ul class="how-we-work-categories">
				<li>
					<a href="#">
						<i class="<?php the_field('work_icon_1');?>"></i>
						<span><?php the_field('work_excerpt_1');?></span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="<?php the_field('work_icon_2');?>"></i>
						<span><?php the_field('work_excerpt_2');?></span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="<?php the_field('work_icon_3');?>"></i>
						<span><?php the_field('work_excerpt_3');?></span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="<?php the_field('work_icon_4');?>"></i>
						<span><?php the_field('work_excerpt_4');?></span>
					</a>
				</li>
			</ul>
		</div>
		<div class="how-we-work-container">
			<div class="research-slope" style="background-image: url(<?php $ResearchBackgroundImage = get_field('work_image_1'); echo $ResearchBackgroundImage['sizes']['ResearchBackgroundImage'];?>);">
				<div class="research shape-content">
					<h3><?php the_field('work_title_1');?></h3>
					<p><?php the_field('work_content_1');?><p>
				</div>
			</div>
			<div class="we-perfection-slope" style="background-image: url(<?php $PerfectionBackgroundImage = get_field('work_image_2'); echo $PerfectionBackgroundImage['sizes']['DesignLaunchBackgroundImage'];?>);">				
				<div class="we-perfection shape-content">
					<h3><?php the_field('work_title_2');?></h3>
					<p><?php the_field('work_content_2');?><p>
				</div>
			</div>
			<div class="car" style="background-image: url(<?php $WorkBackgroundImage = get_field('work_background_image'); echo $WorkBackgroundImage['sizes']['WorkBackgroundImage'];?>);">
			</div>
			<div class="clean-code" style="background-image: url(<?php $CleanBackgroundImage = get_field('work_image_3'); echo $CleanBackgroundImage['sizes']['DevelopBackgroundImage'];?>);">
				<div class="clean-code-box shape-content">
					<h3><?php the_field('work_title_3');?></h3>
					<p><?php the_field('work_content_3');?><p>
				</div>
			</div>
			<div class="we-launch-slope" style="background-image: url(<?php $LaunchBackgroundImage = get_field('work_image_4'); echo $LaunchBackgroundImage['sizes']['DesignLaunchBackgroundImage'];?>);">
				<div class="we-launch shape-content">
					<h3><?php the_field('work_title_4');?></h3>
					<p><?php the_field('work_content_4');?><p>
				</div>
			</div>
		</div>
	</section>
	<?php }  wp_reset_postdata(); ?>	
	<!-- How We Work Over -->
	
	<!-- Apllication Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'apllication',
					);
				
				$frontpageApllication = new WP_Query( $args);
				while ( $frontpageApllication->have_posts()) {
					$frontpageApllication->the_post('apllication'); ?>
	<section id="application-section" class="application-section">
		<div class="container">
			<div class="col-md-6">
				

					<div class="application-iphone">
						<img src="<?php echo  get_the_post_thumbnail_url();?>" alt="application iphone"/>
					</div>
					</div>
					<div class="col-md-6 app-content-box">
					<h2><?php the_title()?></h2>
					<p><?php echo get_the_content() ?></p>
					<div class="col-md-9 app-updates">
						<h4><i class="<?php the_field('icon_box_1');?>"></i><?php the_field('title_box_1');?></h4>
						<p><?php the_field('content_box_1');?></p>
					</div>
					<div class="col-md-9 app-updates">
						<h4><i class="<?php the_field('icon_box_2');?>"></i><?php the_field('title_box_2');?></h4>
						<p><?php the_field('content_box_2');?></p>
					</div>
					<div class="col-md-9 app-updates">
						<h4><i class="<?php the_field('icon_box_3');?>"></i><?php the_field('title_box_3');?></h4>
						<p><?php the_field('content_box_3');?></p>
					</div>
					<div class="col-md-9 app-updates">
						<h4><i class="<?php the_field('icon_box_4');?>"></i><?php the_field('title_box_4');?></h4>
						<p><?php the_field('content_box_4');?></p>
					</div>
					
					<!-- <div class="col-md-9 app-updates">
						<h4><i class="icon_lightbulb"></i> Free Updates</h4>
						<p>Curabitur arcu erat, accusaimp eret et porttitor at sesdabx auisque velit nisi pret</p>
					</div> -->
					
			</div>
		</div>
	</section>
	<?php }  wp_reset_postdata(); ?>
	<!-- Apllication Section Over -->
	
	<!-- Our Genius -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'genius',
					);
				
				$frontpageGenius = new WP_Query( $args);
				while ( $frontpageGenius->have_posts()) {
					$frontpageGenius->the_post('genius'); ?>
	<section id="our-genius" class="our-genius">
		<!-- Section Header -->
		<div class="section-header">
			<h2><?php the_title();?></h2>
		</div>
		<!-- Section Header Over-->
		<div class="genius-gallery">		
			<div id="style1" class="style1 style-active">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon1">
						<polygon points="0 424,491 500,415 0,0 0">
						</polygon>
					</clipPath>					
				</svg>
				<!-- Group Start Social Genius-->
				<?php $socialGenius = get_field('social_genius');	
					if( $socialGenius ): ?>
						<img src="<?php echo $socialGenius['genius_image']['sizes']['GeniusGalleryImage'];?>" alt="<?php echo $socialGenius['genius_image']['genius'];?>"/>
						<div class="hover"><?php echo $socialGenius['social_name'];?></div>
				<?php endif; ?>
				<!-- Group End Social Genius-->
			</div>
			<div id="style2" class="style2">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon2">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>
				<img src="<?php echo  get_the_post_thumbnail_url();?>" alt="genius"/>				
			</div>
			<div id="style3" class="style3">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon3">
						<polygon points="0 400,480 400,480 0,53 0">
						</polygon>
					</clipPath>
				</svg>
				<!-- Group Start Social Genius-->
				<?php $socialGenius2 = get_field('social_genius_2');	
					if( $socialGenius2 ): ?>
						<img src="<?php echo $socialGenius2['genius_image_2']['sizes']['GeniusGalleryImage'];?>" alt="<?php echo $socialGenius2['genius_image_2']['genius'];?>"/>
						<div class="hover"><?php echo $socialGenius2['social_name_2'];?></div>
				<?php endif; ?>
				<!-- Group End Social Genius-->
			</div>
			<div id="style4" class="style4">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon4">
						<polygon points="0 405,526 400,476 0,0 0">
						</polygon>
					</clipPath>
				</svg>
				<!-- Group Start Social Genius-->
				<?php $socialGenius3 = get_field('social_genius_3');	
					if( $socialGenius3 ): ?>
						<img src="<?php echo $socialGenius3['genius_image_3']['sizes']['GeniusGalleryImage'];?>" alt="<?php echo $socialGenius3['genius_image_3']['genius'];?>"/>
						<div class="hover"><?php echo $socialGenius3['social_name_3'];?></div>
				<?php endif; ?>
				<!-- Group End Social Genius-->
			</div>
			<div id="style5" class="style5">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon5">
						<polygon points="50 402,480 400,480 0,5 0">
						</polygon>
					</clipPath>
				</svg>
				<!-- Group Start Social Genius-->
				<?php $socialGenius4 = get_field('social_genius_4');	
					if( $socialGenius4 ): ?>
						<img src="<?php echo $socialGenius4['genius_image_4']['sizes']['GeniusGalleryImage'];?>" alt="<?php echo $socialGenius4['genius_image_4']['genius'];?>"/>
						<div class="hover"><?php echo $socialGenius4['social_name_4'];?></div>
				<?php endif; ?>
				<!-- Group End Social Genius-->
			</div>
			<div id="style6" class="style6">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon6">
						<polygon points="0 400,430 400,473 0,0 0">
						</polygon>
					</clipPath>
				</svg>
				<!-- Group Start Social Genius-->
				<?php $socialGenius5 = get_field('social_genius_5');	
					if( $socialGenius5 ): ?>
						<img src="<?php echo $socialGenius5['genius_image_5']['sizes']['GeniusGalleryImage'];?>" alt="<?php echo $socialGenius5['genius_image_5']['genius'];?>"/>
						<div class="hover"><?php echo $socialGenius5['social_name_5'];?></div>
				<?php endif; ?>
				<!-- Group End Social Genius-->
			</div>
			<div id="style7" class="style7">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon7">
						<polygon points="0 400,530 404,530 0,50 0">
						</polygon>
					</clipPath>
				</svg>
				<!-- Group Start Social Genius-->
				<?php $socialGenius6 = get_field('social_genius_6');	
					if( $socialGenius6 ): ?>
						<img src="<?php echo $socialGenius6['genius_image_6']['sizes']['GeniusGalleryImage'];?>" alt="<?php echo $socialGenius6['genius_image_6']['genius'];?>"/>
						<div class="hover"><?php echo $socialGenius6['social_name_6'];?></div>
				<?php endif; ?>
				<!-- Group End Social Genius-->
			</div>
			
			<div id="style1-hover" class="style2 member-info">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon13">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>				
				<div class="col-md-6 social-info">
					<div class="inner-social-info">
						<!-- Group Start Social Genius-->
						<?php $socialGenius = get_field('social_genius');	
							if( $socialGenius ): ?>
									<h3><?php echo $socialGenius['social_name']; ?></h3>
									<h6><?php echo $socialGenius['social_job']; ?></h6>
									<div class="social-info-content1">
										<p><?php echo $socialGenius['social_content']; ?></p>
									</div>
									<ul>
										<!-- Start Social Genius Loop -->
										<?php if( have_rows('social_genius') ): 
											while( have_rows('social_genius') ): the_row();
													 // Start Repeater Socilal Link
													if( have_rows('socilal_link') ):
													    while ( have_rows('socilal_link') ) : the_row(); ?>
														<li><a href="<?php the_sub_field('link_soc');?>"><i class="<?php the_sub_field('icon_soc');?>"></i></a></li>
													<?php  endwhile;
													else :
													    // no rows found
													endif;
													// End Repeater Socilal Link
												 endwhile; 
										 endif; ?>
										<!-- End Social Genius Loop -->
									</ul>
						<?php endif; ?>
						<!-- Group End Social Genius-->
					</div>
				</div>
				<div class="col-md-6 skills-info">
					<!-- Start Social Genius Loop -->
					<?php if( have_rows('social_genius') ): 
							while( have_rows('social_genius') ): the_row();
								 //Start Repeater Skill Progress 
								 if( have_rows('skill_progress') ):
										while ( have_rows('skill_progress') ) : the_row(); ?>
											<div class="skill-progress-box">
												<div class="progress">
													<div id="skill_bar_count" class="progress-bar progress-bar-danger html" role="progressbar" style="width: <?php the_sub_field('skill_range');?>%">
													</div>
													<h6><?php the_sub_field('skill_title');?><Span id="skill_count" data-skills_percent="<?php $range = get_sub_field('skill_range');?><?php if( $range ):?> <?php echo $range;?><?php endif;?>"></span></h6>
												</div>
											</div>	
								<?php    endwhile;
								else :
								 // no rows found
								endif; 
								// End Repeater Skill Progress 
						    endwhile; 
						endif; ?>
						<!-- End Social Genius Loop -->    		
				</div>
			</div>
			
			<div id="style3-hover" class="style2 member-info">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon8">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>				
				<div class="col-md-6 social-info">
					<div class="inner-social-info">
						<!-- Group Start Social Genius-->
						<?php $socialGenius2 = get_field('social_genius_2');	
							if( $socialGenius2 ): ?>
									<h3><?php echo $socialGenius2['social_name_2']; ?></h3>
									<h6><?php echo $socialGenius2['social_job_2']; ?></h6>
									<div class="social-info-content1">
										<p><?php echo $socialGenius2['social_content_2']; ?></p>
									</div>
									<ul>
										<!-- Start Social Genius Loop -->
										<?php if( have_rows('social_genius_2') ): 
											while( have_rows('social_genius_2') ): the_row();
													 // Start Repeater Socilal Link
													if( have_rows('socilal_link_2') ):
													    while ( have_rows('socilal_link_2') ) : the_row(); ?>
														<li><a href="<?php the_sub_field('link_soc_2');?>"><i class="<?php the_sub_field('icon_soc_2');?>"></i></a></li>
													<?php  endwhile;
													else :
													    // no rows found
													endif;
													// End Repeater Socilal Link
												 endwhile; 
										 endif; ?>
										<!-- End Social Genius Loop -->
									</ul>
						<?php endif; ?>
						<!-- Group End Social Genius-->
					</div>
				</div>
				<div class="col-md-6 skills-info">					
					<!-- Start Social Genius Loop -->
					<?php if( have_rows('social_genius_2') ): 
							while( have_rows('social_genius_2') ): the_row();
								 //Start Repeater Skill Progress 
								 if( have_rows('skill_progress_2') ):
										while ( have_rows('skill_progress_2') ) : the_row(); ?>
											<div class="skill-progress-box">
												<div class="progress">
													<div id="skill_bar_count" class="progress-bar progress-bar-danger html" role="progressbar" style="width: <?php the_sub_field('skill_range_2');?>%">
													</div>
													<h6><?php the_sub_field('skill_title_2');?><Span id="skill_count" data-skills_percent="<?php $range = get_sub_field('skill_range_2');?><?php if( $range ):?> <?php echo $range;?><?php endif;?>"></span></h6>
												</div>
											</div>	
								<?php    endwhile;
								else :
								 // no rows found
								endif; 
								// End Repeater Skill Progress 
						    endwhile; 
						endif; ?>
						<!-- End Social Genius Loop -->
				</div>
			</div>
			
			<div id="style4-hover" class="style2 member-info">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon9">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>				
				<div class="col-md-6 social-info">
					<div class="inner-social-info">
						<!-- Group Start Social Genius-->
						<?php $socialGenius3 = get_field('social_genius_3');	
							if( $socialGenius3 ): ?>
									<h3><?php echo $socialGenius3['social_name_3']; ?></h3>
									<h6><?php echo $socialGenius3['social_job_3']; ?></h6>
									<div class="social-info-content1">
										<p><?php echo $socialGenius3['social_content_3']; ?></p>
									</div>
									<ul>
										<!-- Start Social Genius Loop -->
										<?php if( have_rows('social_genius_3') ): 
											while( have_rows('social_genius_3') ): the_row();
													 // Start Repeater Socilal Link
													if( have_rows('socilal_link_3') ):
													    while ( have_rows('socilal_link_3') ) : the_row(); ?>
														<li><a href="<?php the_sub_field('link_soc_3');?>"><i class="<?php the_sub_field('icon_soc_3');?>"></i></a></li>
													<?php  endwhile;
													else :
													    // no rows found
													endif;
													// End Repeater Socilal Link
												 endwhile; 
										 endif; ?>
										<!-- End Social Genius Loop -->
									</ul>
						<?php endif; ?>
						<!-- Group End Social Genius-->
					</div>
				</div>
				<div class="col-md-6 skills-info">					
					<!-- Start Social Genius Loop -->
					<?php if( have_rows('social_genius_3') ): 
							while( have_rows('social_genius_3') ): the_row();
								 //Start Repeater Skill Progress 
								 if( have_rows('skill_progress_3') ):
										while ( have_rows('skill_progress_3') ) : the_row(); ?>
											<div class="skill-progress-box">
												<div class="progress">
													<div id="skill_bar_count" class="progress-bar progress-bar-danger html" role="progressbar" style="width: <?php the_sub_field('skill_range_3');?>%">
													</div>
													<h6><?php the_sub_field('skill_title_3');?><Span id="skill_count" data-skills_percent="<?php $range = get_sub_field('skill_range_3');?><?php if( $range ):?> <?php echo $range;?><?php endif;?>"></span></h6>
												</div>
											</div>	
								<?php    endwhile;
								else :
								 // no rows found
								endif; 
								// End Repeater Skill Progress 
						    endwhile; 
						endif; ?>
						<!-- End Social Genius Loop -->
				</div>
			</div>
			
			<div id="style5-hover" class="style2 member-info">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon10">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>				
				<div class="col-md-6 social-info">
					<div class="inner-social-info">
						<!-- Group Start Social Genius-->
						<?php $socialGenius4 = get_field('social_genius_4');	
							if( $socialGenius4 ): ?>
									<h3><?php echo $socialGenius4['social_name_4']; ?></h3>
									<h6><?php echo $socialGenius4['social_job_4']; ?></h6>
									<div class="social-info-content1">
										<p><?php echo $socialGenius4['social_content_4']; ?></p>
									</div>
									<ul>
										<!-- Start Social Genius Loop -->
										<?php if( have_rows('social_genius_4') ): 
											while( have_rows('social_genius_4') ): the_row();
													 // Start Repeater Socilal Link
													if( have_rows('socilal_link_4') ):
													    while ( have_rows('socilal_link_4') ) : the_row(); ?>
														<li><a href="<?php the_sub_field('link_soc_4');?>"><i class="<?php the_sub_field('icon_soc_4');?>"></i></a></li>
													<?php  endwhile;
													else :
													    // no rows found
													endif;
													// End Repeater Socilal Link
												 endwhile; 
										 endif; ?>
										<!-- End Social Genius Loop -->
									</ul>
						<?php endif; ?>
						<!-- Group End Social Genius-->
					</div>
				</div>
				<div class="col-md-6 skills-info">					
					<!-- Start Social Genius Loop -->
					<?php if( have_rows('social_genius_4') ): 
							while( have_rows('social_genius_4') ): the_row();
								 //Start Repeater Skill Progress 
								 if( have_rows('skill_progress_4') ):
										while ( have_rows('skill_progress_4') ) : the_row(); ?>
											<div class="skill-progress-box">
												<div class="progress">
													<div id="skill_bar_count" class="progress-bar progress-bar-danger html" role="progressbar" style="width: <?php the_sub_field('skill_range_4');?>%">
													</div>
													<h6><?php the_sub_field('skill_title_4');?><Span id="skill_count" data-skills_percent="<?php $range = get_sub_field('skill_range_4');?><?php if( $range ):?> <?php echo $range;?><?php endif;?>"></span></h6>
												</div>
											</div>	
								<?php    endwhile;
								else :
								 // no rows found
								endif; 
								// End Repeater Skill Progress 
						    endwhile; 
						endif; ?>
						<!-- End Social Genius Loop -->
				</div>
			</div>
			
			<div id="style6-hover" class="style2 member-info">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon11">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>				
				<div class="col-md-6 social-info">
					<div class="inner-social-info">
						<!-- Group Start Social Genius-->
						<?php $socialGenius5 = get_field('social_genius_5');	
							if( $socialGenius5 ): ?>
									<h3><?php echo $socialGenius5['social_name_5']; ?></h3>
									<h6><?php echo $socialGenius5['social_job_5']; ?></h6>
									<div class="social-info-content1">
										<p><?php echo $socialGenius5['social_content_5']; ?></p>
									</div>
									<ul>
										<!-- Start Social Genius Loop -->
										<?php if( have_rows('social_genius_5') ): 
											while( have_rows('social_genius_5') ): the_row();
													 // Start Repeater Socilal Link
													if( have_rows('socilal_link_5') ):
													    while ( have_rows('socilal_link_5') ) : the_row(); ?>
														<li><a href="<?php the_sub_field('link_soc_5');?>"><i class="<?php the_sub_field('icon_soc_5');?>"></i></a></li>
													<?php  endwhile;
													else :
													    // no rows found
													endif;
													// End Repeater Socilal Link
												 endwhile; 
										 endif; ?>
										<!-- End Social Genius Loop -->
									</ul>
						<?php endif; ?>
						<!-- Group End Social Genius-->
					</div>
				</div>
				<div class="col-md-6 skills-info">					
					<!-- Start Social Genius Loop -->
					<?php if( have_rows('social_genius_5') ): 
							while( have_rows('social_genius_5') ): the_row();
								 //Start Repeater Skill Progress 
								 if( have_rows('skill_progress_5') ):
										while ( have_rows('skill_progress_5') ) : the_row(); ?>
											<div class="skill-progress-box">
												<div class="progress">
													<div id="skill_bar_count" class="progress-bar progress-bar-danger html" role="progressbar" style="width: <?php the_sub_field('skill_range_5');?>%">
													</div>
													<h6><?php the_sub_field('skill_title_5');?><Span id="skill_count" data-skills_percent="<?php $range = get_sub_field('skill_range_5');?><?php if( $range ):?> <?php echo $range;?><?php endif;?>"></span></h6>
												</div>
											</div>	
								<?php    endwhile;
								else :
								 // no rows found
								endif; 
								// End Repeater Skill Progress 
						    endwhile; 
						endif; ?>
						<!-- End Social Genius Loop -->
				</div>
			</div>
			
			<div id="style7-hover" class="style2 member-info">
				<svg width="100%" height="100%">
					<clipPath id="clipPolygon12">
						<polygon points="80 532,1005 417,1061 -3,0 0">
						</polygon>
					</clipPath>
				</svg>				
				<div class="col-md-6 social-info">
					<div class="inner-social-info">
						<!-- Group Start Social Genius-->
						<?php $socialGenius6 = get_field('social_genius_6');	
							if( $socialGenius6 ): ?>
									<h3><?php echo $socialGenius6['social_name_6']; ?></h3>
									<h6><?php echo $socialGenius6['social_job_6']; ?></h6>
									<div class="social-info-content1">
										<p><?php echo $socialGenius6['social_content_6']; ?></p>
									</div>
									<ul>
										<!-- Start Social Genius Loop -->
										<?php if( have_rows('social_genius_6') ): 
											while( have_rows('social_genius_6') ): the_row();
													 // Start Repeater Socilal Link
													if( have_rows('socilal_link_6') ):
													    while ( have_rows('socilal_link_6') ) : the_row(); ?>
														<li><a href="<?php the_sub_field('link_soc_6');?>"><i class="<?php the_sub_field('icon_soc_6');?>"></i></a></li>
													<?php  endwhile;
													else :
													    // no rows found
													endif;
													// End Repeater Socilal Link
												 endwhile; 
										 endif; ?>
										<!-- End Social Genius Loop -->
									</ul>
						<?php endif; ?>
						<!-- Group End Social Genius-->
					</div>
				</div>
				<div class="col-md-6 skills-info">					
					<!-- Start Social Genius Loop -->
					<?php if( have_rows('social_genius_6') ): 
							while( have_rows('social_genius_6') ): the_row();
								 //Start Repeater Skill Progress 
								 if( have_rows('skill_progress_6') ):
										while ( have_rows('skill_progress_6') ) : the_row(); ?>
											<div class="skill-progress-box">
												<div class="progress">
													<div id="skill_bar_count" class="progress-bar progress-bar-danger html" role="progressbar" style="width: <?php the_sub_field('skill_range_6');?>%">
													</div>
													<h6><?php the_sub_field('skill_title_6');?><Span id="skill_count" data-skills_percent="<?php $range = get_sub_field('skill_range_6');?><?php if( $range ):?> <?php echo $range;?><?php endif;?>"></span></h6>
												</div>
											</div>	
								<?php    endwhile;
								else :
								 // no rows found
								endif; 
								// End Repeater Skill Progress 
						    endwhile; 
						endif; ?>
						<!-- End Social Genius Loop -->
				</div>
			</div>
		</div>
	</section>
	<?php }  wp_reset_postdata(); ?>
	<!-- Our Genius Over -->
	
	<!-- Video Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'muve',
					);
				
				$frontpageMuve = new WP_Query( $args);
				while ( $frontpageMuve->have_posts()) {
					$frontpageMuve->the_post('muve');?>
	<div id="video-section" class="video-section ">
		<a id="bgndVideo" class="player" data-property="{videoURL:'<?php the_field('muve_video'); ?>',containment:'.video-section', showControls:true, autoPlay:true, loop:true, vol:50, mute:true, startAt:10, opacity:1, addRaster:true, quality:'default', optimizeDisplay:true}"></a> <!--BsekcY04xvQ-->
	</div>
	<?php } wp_reset_postdata();
			
		?>

	<!-- Video Section Over -->
	
	<!-- Statistics Section -->
	<div id="statistics-section" class="statistics-section">
		<div class="container">
			<div class="col-md-3 col-sm-12">
				<h1><i class="icon_compass"></i> <span id="project"></span>K</h1>
				<p>PROJECT COMPLETE</p>
			</div>
			<div class="col-md-3 col-sm-12">
				<h1><i class="icon_toolbox "></i> <span id="work"></span> &nbsp; </h1>
				<p>CLIENT WORKED WITH</p>
			</div>
			<div class="col-md-3 col-sm-12">
				<h1><i class="icon_mug_alt"></i> <span id="consumed"></span> &nbsp; </h1>
				<p>CUPS OF COFFEE CONSUMED</p>
			</div>
			<div class="col-md-3 col-sm-12">
				<h1><i class="icon_shield_alt"></i> <span id="videos"></span> &nbsp; </h1>
				<p>ANSWERED QUESTIONS</p>
			</div>
		</div>
	</div>
	<!-- Statistics Section Over -->
	
	<!-- Blog Post -->
	<section id="blog-section" class="blog-section">
		<!-- Section Header -->
		<div class="section-header">
			<h2>latest posts</h2>
		</div>
		
		<!-- Section Header Over-->
		<div class="container no-padding">
			<div class="blog-inner">
				<?php 
				/*
				 * The WordPress Query class.
				 *
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 */
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
					'posts_per_page'=> 3,
				));
			
			while ( $frontpagePosts->have_posts()) {
				$frontpagePosts->the_post(); ?>
				<div class="col-md-4 col-sm-6 post">
					<div class="entry-date">
						<div class="dates">
							<h2><?php the_date('d')?></h2>
							<h4><?php echo get_the_date('M')?></h4>
						</div>
						<div class="icon">
							<i class="fa fa-heart"></i>
							<i class="fa fa-comments"></i>
						</div>
					</div>
					<div class="entry-cover">
						<img src="<?php echo the_post_thumbnail_url('BlogPostCoverImage'); ?>" alt="cover image"/>
						<div class="entry-header">
							<a href="<?php the_permalink();?>" class="entry-title"><?php the_title();?></a>
						</div>
						<div class="post-date">
							<span class="entry-date"><?php echo get_the_date('F j, Y');?></span>
							<span class="comments-link"><a href="#"><?php echo get_comments_number()?> COMMENTS</a></span>
							<?php
							              $likeCount = new WP_Query(array(
							                'post_type' => 'like',
							                'meta_query' => array(
							                  array(
							                    'key' => 'liked_post_id',
							                    'compare' => '=',
							                    'value' => get_the_ID()
							                  )
							                )
							              )); 
							              ?>
					<span class="post-views"><i class="fa fa-heart"></i> <span class="like-count"><?php echo $likeCount->found_posts;?></span></span>
						</div>
						<p class="post-item"><?php echo wp_trim_words(get_the_content(), '12'); ?></p>
						<a href="<?php the_permalink();?>" class="read-more">read more</a>
					</div>
				</div>
				
			<?php } wp_reset_postdata();
			
		?>
				
			</div>
		</div>
		
	</section>
	<!-- Blog Post Over -->
		
	<!-- What Clients Section -->
	<section id="client-section" class="client-section">
		<!-- Section Header -->
		<div class="section-header">
			<h2>WHAT CLIENTS SAY</h2>
		</div>
		<!-- Section Header Over-->
		<?php 
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
					'name'=> 'element-of-my-life',
				));
			
			while ( $frontpagePosts->have_posts()) {
				$frontpagePosts->the_post(); ?>
		<div class="client-style1" style="background-image: url(
		<?php if (page_banner_background_image) {$PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['ClientsSectionStyle1'];} ?>);">
			<svg width="0" height="0">
				<clipPath id="client-1">
					<polygon points="0 152,1920 310,1920 0,0 0">
					</polygon>
				</clipPath>
				<clipPath id="client-11">
					<polygon points="0 752,1920 752,1920 0,0 0">
					</polygon>
				</clipPath>
			</svg>
			<div class="client-hover">
				<?php
				$args = array(
				  'post_name'           => 'element-of-my-life',
				  // 'id'					=> 'comment-4',
				  'date_query' => array(
				  	'hour'      => '7',
					'minute'    => '42',
				  ),
				);
				// The Query
				$comments_query = new WP_Comment_Query;
				$comments = $comments_query->query( $args );

				// Comment Loop
					foreach ( $comments as $comment ) {
						echo '<h2>' . $comment->comment_content . '</h2>';
						echo '<p>' . $comment->comment_author . '</p>'; 
					}
				?>	
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>
		<?php 
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
					'name'=> 'photohop-hardcore',
				));
			
			while ( $frontpagePosts->have_posts()) {
				$frontpagePosts->the_post(); ?>
		<div class="client-style2 client-active" style="background-image: url(
		<?php $PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['ClientsSectionStyle2'];?>);">	
			<svg width="0" height="0">
				<clipPath id="client-2">
					<polygon points="-1 450,1920 289,1920 158,0 0">
					</polygon>
				</clipPath>
				<clipPath id="client-21">
					<polygon points="-1 752,1920 666,1920 97,0 0">
					</polygon>
				</clipPath>
			</svg>			
			<div class="client-hover">
				<?php
				$args = array(
				  'post_name'           => 'photohop-hardcore',
				  // 'id'					=> 'comment-4',
				  'date_query' => array(
				  	'hour'      => '7',
					'minute'    => '44',
				  ),
				);
				// The Query
				$comments_query = new WP_Comment_Query;
				$comments = $comments_query->query( $args );

				// Comment Loop
					foreach ( $comments as $comment ) {
						echo '<h2>' . $comment->comment_content . '</h2>';
						echo '<p>' . $comment->comment_author . '</p>';
					}
				?>	
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>
		<?php 
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
					'name'=> 'element-of-my-life',
				));
			
			while ( $frontpagePosts->have_posts()) {
				$frontpagePosts->the_post(); ?>
		<div class="client-style3" style="background-image: url(
			<?php $PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['ClientsSectionStyle3'];?>);">
			<svg width="0" height="0">
				<clipPath id="client-3">
					<polygon points="-1 326,1920 316,1920 0,0 155">
					</polygon>
				</clipPath>
				<clipPath id="client-31">
					<polygon points="-1 752,1920 756,1920 0,0 155">
					</polygon>
				</clipPath>
			</svg>
			<div class="client-hover">
				<?php
				$args = array(
				  'post_name'           => 'element-of-my-life',
				  // 'id'					=> 'comment-4',
				  'date_query' => array(
				  	'hour'      => '7',
					'minute'    => '53',
				  ),
				);
				// The Query
				$comments_query = new WP_Comment_Query;
				$comments = $comments_query->query( $args );

				// Comment Loop
					foreach ( $comments as $comment ) {
						echo '<h2>' . $comment->comment_content . '</h2>';
						echo '<p>' . $comment->comment_author . '</p>';
					}
				?>	
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>		
	</section>

	<!-- What Clients Section Over -->
	
	<!-- Brag About Section -->
	<div id="brag-about-section" class="brag-about-section">
		<div class="col-md-6 brag-about-img pull-right">
			<img src="<?php echo get_theme_file_uri('/images/about/about-imac.png')?>" alt="iMac">
		</div>	
		<div class="col-md-6 no-padding">
			<div class="col-md-7 pull-right brag-content">
				<h1>design to brag about</h1>
				<p>Morbi in velit, eget vulputate ligula viverra, mi ae sollicitudin rhon roin gravida nibh vel velit auctor aliquet. Aeneanollic itudin lorem dumorbi in velit ligula, eget vulputate ligula viverra, mi ae sollicit rhoncusante roin gravida nibh vel velit auctor aliquetc</p>
			</div>
		</div>
	</div>
	<!-- Brag About Section Over -->
	
	<!-- Social Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'contact',
					);
				
				$frontpageContact = new WP_Query( $args);
				while ( $frontpageContact->have_posts()) {
					$frontpageContact->the_post('contact');?>

	<div id="social-section" class="social-main">
		<?php
			if( have_rows('socilal_link_contact') ):
			    while ( have_rows('socilal_link_contact') ) : the_row();?>
			        <div class="no-padding">
						<a href="<?php the_sub_field('link_soc_contact');?>" style="background-color:<?php the_sub_field('social_color'); ?>"><i class="<?php the_sub_field('icon_soc_contact');?>" ></i></a>
					</div>
				<?php
			    endwhile;
			else :
			    // no rows found
			endif;
			?>
	</div>
	<?php } wp_reset_postdata();
			
	?>
	<!-- Social Section Over -->
	
	<!-- Map Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'contact',
					);
				
				$frontpageContact = new WP_Query( $args);
				while ( $frontpageContact->have_posts()) {
					$frontpageContact->the_post('contact');?>
					
	<div id="map-section" class="map-section">
		<?php 

				$location = get_field('map_section'); 
				if( !empty($location) ):
				?>
				<div class="acf-map">
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
				<?php endif; ?>
	</div>

	<?php } wp_reset_postdata();
			
	?>
	<!-- Map Section Over -->
	
	<!-- Project Section -->
	<div id="project-section" class="project-section">
		<div class="container">
			<div class="col-md-6 col-sm-6 col-xs-6 start-project">
				<p>have you fallen in love yet?</p>
				<h2>start a project</h2>
				<a href="#"><span>LEt's Go</span></a>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6 send-msg">
				<p>Want to tell us something?</p>
				<h2>Send a message</h2>
				<a href="#" data-toggle="modal" data-target="#myModal"><span>Tell Us</span></a>
			</div>
		</div>
	</div>
	<!-- Project Section Over -->
<?php get_footer(); ?>