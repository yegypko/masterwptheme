<?php get_header();?>
		<!-- LOADER -->
		<!-- <div id="site-loader" class="load-complete">
			<div class="load-position">
				<div class="logo"><img src="images/loading-logo.png" alt="loading-logo"/></div>
				<h6>Please wait, loading...</h6>
				<div class="loading">
					<div class="loading-line"></div>
					<div class="loading-break loading-dot-1"></div>
					<div class="loading-break loading-dot-2"></div>
					<div class="loading-break loading-dot-3"></div>
				</div>
			</div>
		</div> -->

		<?php 
				
				$blogBanner = new WP_Query (array(
					'post_type'   => 'page',
					'name'        => 'blog',
				));
			while ( $blogBanner->have_posts()) {
				$blogBanner->the_post(); ?>


			<div class="page-banner container-fluid">
					<div class="header-top" style="background-image: url(<?php $PageBannerBackground_Image = get_field('page_banner_background_image'); echo $PageBannerBackground_Image['sizes']['PageBannerBackgroundImage'];?>);"> 
						<!-- <img src="<?php the_post_thumbnail('sliderImage');?>"> -->
					</div>
					<div class="page-banner__content heading header-top">
						<h1><?php the_title();?></h1>
						<!-- <h1>My Blog</h1> -->
				  		 <p class="heading-description"><?php the_field('page_banner_subtitle')?></p> 
					</div>
			</div>

		<?php } ?>

		<div class="blog-page">
			<div class="container no-padding">
				<div class="blog-inner">

					<?php
						while(have_posts()) {
							the_post(); ?>

							<div class="col-md-4 col-sm-6 post">
								<div class="entry-date">
									<div class="dates">
										<h2><?php the_date('d')?></h2>
										<h4><?php echo get_the_date('M')?></h4>
									</div>
									<div class="icon">
										<i class="fa fa-heart"></i>
										<i class="fa fa-comments"></i>
									</div>
								</div>
								<div class="entry-cover">
									<img src="<?php echo the_post_thumbnail_url('BlogPostCoverImage'); ?>" alt="cover image"/>
									<div class="entry-header">
										<a href="<?php the_permalink();?>" class="entry-title"><?php the_title();?></a>
									</div>
									<div class="post-date">
										<span class="entry-date"><?php echo get_the_date('F j, Y');?></span>
										<span class="comments-link"><a href="#"><?php echo get_comments_number()?> COMMENTS</a></span>
										<?php
							              $likeCount = new WP_Query(array(
							                'post_type' => 'like',
							                'meta_query' => array(
							                  array(
							                    'key' => 'liked_post_id',
							                    'compare' => '=',
							                    'value' => get_the_ID()
							                  )
							                )
							              )); 
							              ?>
										<span class="post-views"><i class="fa fa-heart"></i> <span class="like-count"><?php echo $likeCount->found_posts;?></span></span>
									</div>
									<p class="post-item"><?php echo wp_trim_words(get_the_content(), '12');?></p>
									<a href="<?php the_permalink();?>" class="read-more">read more</a>
								</div>
							</div>

						<?php } 
					?>
					
				</div>
				<?php echo paginate_links(); ?>
			</div>
		</div>

	


<?php get_footer(); ?>
