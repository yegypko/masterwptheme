<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Master_WP_Theme
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function master_wp_theme_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'master_wp_theme_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function master_wp_theme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
	
}
add_action( 'wp_head', 'master_wp_theme_pingback_header' );



// Custom Comments
 function custom_comments( $comment, $args, $depth ) {
			$GLOBALS['comment'] = $comment;
			switch( $comment->comment_type ) :
			case 'pingback' :
			case 'trackback' : ?>
				<li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
					<div class="back-link">< ?php comment_author_link(); ?></div>
						<?php break;
						 default : ?>
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
				<ul <?php comment_class(); ?> class="media-list">
					<li class="media">
						<div class="media-left">
							<?php echo get_avatar($comment, 60); ?>				
						</div>
						<div class="media-body">
							<h4 class="media-heading"><?php comment_author(); ?><span>| 
<?php printf( _x( '%s ago', '%s = human-readable time difference', 'your-text-domain' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) ); ?>

</span></h4>
							<p><?php comment_text(); ?></p>
							<a href="#"><?php comment_reply_link( array_merge( 
											$args, array( 
														'reply_text' => 'Reply',
														// 'after' => ' <span></span>', 
														'depth' => $depth,
														'max_depth' => $args['max_depth'] 
														) ) ); ?>
							</a>					
						</div>
					</li>							
				</ul>

			<?php // End the default styling of comment
			break;
			endswitch;
}
	
add_action('commentFunctions', 'custom_comments');





// Custom Like


add_action('rest_api_init', 'masterWpLikeRoutes');

function masterWpLikeRoutes() {
  register_rest_route('masterwp/v1', 'manageLike', array(
    'methods' => 'POST',
    'callback' => 'createLike'
  ));

  register_rest_route('masterwp/v1', 'manageLike', array(
    'methods' => 'DELETE',
    'callback' => 'deleteLike'
  ));
}


function createLike($data) {
	if (is_user_logged_in()) {
		$postLike = sanitize_text_field($data['postId']);
		$existQuery = new WP_Query(array(
					                  'author' => get_current_user_id(),
					                  'post_type' => 'like',
					                  'meta_query' => array(
					                    array(
					                      'key' => 'liked_post_id',
					                      'compare' => '=',
					                      'value' => $postLike
					                    )
					                  )
					                ));
		if ($existQuery->found_posts == 0 AND get_post_type($postLike) == 'post') {
			return wp_insert_post(
			array(
	        'post_type' => 'like',
	        'post_status' => 'publish',
	        'post_title' => 'Created Likes',
	        'meta_input' => array(
	        	'liked_post_id' => $postLike
	        )
			)
		);
		} else {
			die("Invalid post id");
		}
		
		
	} else {
		die("Only logged in users can create a like.");
	}
	
	
}

function deleteLike($data) {
  $likeId = sanitize_text_field($data['like']);
  if (get_current_user_id() == get_post_field('post_author', $likeId) AND get_post_type($likeId) == 'like') {
    wp_delete_post($likeId, true);
    return 'Congrats, like deleted.';
  } else {
    die("You do not have permission to delete that.");
  }
}



