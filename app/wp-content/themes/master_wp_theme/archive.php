<?php get_header();?>
		<!-- LOADER -->
		<!-- <div id="site-loader" class="load-complete">
			<div class="load-position">
				<div class="logo"><img src="images/loading-logo.png" alt="loading-logo"/></div>
				<h6>Please wait, loading...</h6>
				<div class="loading">
					<div class="loading-line"></div>
					<div class="loading-break loading-dot-1"></div>
					<div class="loading-break loading-dot-2"></div>
					<div class="loading-break loading-dot-3"></div>
				</div>
			</div>
		</div> -->
		<div class="page-banner container-fluid">
				<div class="header-top" style="background-image: url(<?php $PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['PageBannerBackgroundImage'];?>);"> 
				</div>
				<div class="page-banner__content heading header-top">
					<h1><?php the_archive_title( $before = '', $after = '' );
				?></h1>
			  		 <p class="heading-description"><?php the_field('page_banner_subtitle')?></p> 
				</div>
		</div>

		<!-- <div class="container-fluid no-padding">
			<div class="beautiful-image">
				<img src="<?php echo get_theme_file_uri('/images/bg.jpg')?>" alt="bg">
				<h2 class="heading"><?php
					if (is_category()) {
						echo 'Category is '; single_cat_title();
					}if (is_author()) {
						echo 'Posts by '; the_author();
					}
					the_archive_title( $before = '', $after = '' );
				?>
				</h2>
				<p class="heading-description"><?php the_archive_description()?></p>
			</div>
		</div> -->
		<div class="single-blog">
		<div class="container">
			<?php while(have_posts()) {
				the_post(); ?>
					
					<div class="col-md-10">
						<div class="col-md-7 col-sm-6 no-l-padding by-admin">
							<p> By <?php the_author_posts_link();?> | <?php echo get_the_date('F j, Y');?> | 5 Comments</p>
						</div>
						 <!-- <div class="entry-cover">

							 <img src="<?php echo get_theme_file_uri('/images/images-1.jpg')?>" alt="image 1"> 
						</div>  -->
						<div class="entery-content">
							<a href="<?php the_permalink();?>"><h3><?php the_title();?></h3></a>
							<p><?php the_content();?></p>
						</div>
					</div>
					
			<?php }?>
		</div>
	</div>

	


<?php get_footer(); ?>