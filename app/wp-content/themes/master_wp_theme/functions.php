<?php
/**
 * Master WP Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Master_WP_Theme
 */

if ( ! function_exists( 'master_wp_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function master_wp_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Master WP Theme, use a find and replace
		 * to change 'master_wp_theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'master_wp_theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );


		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'sliderImage', $width = 1920, $height = 1020, $crop = true );
		add_image_size( 'PageBannerBackgroundImage', $width = 1920, $height = 500, $crop = true );
		add_image_size( 'ServicesBackgroundImage', $width = 450, $height = 450, $crop = true );
		add_image_size( 'FeaturesBackgroundImage', $width = 1920, $height = 1020, $crop = true );
		add_image_size( 'PortfolioImage', $width = 480, $height = 400, $crop = true );
		add_image_size( 'WorkBackgroundImage', $width = 1920, $height = 950, $crop = true );
		add_image_size( 'ResearchBackgroundImage', $width = 1920, $height = 887, $crop = true );
		add_image_size( 'DesignLaunchBackgroundImage', $width = 1920, $height = 800, $crop = true );
		add_image_size( 'DevelopBackgroundImage', $width = 1920, $height = 803, $crop = true );
		add_image_size( 'GeniusGalleryImage', $width = 480, $height = 400, $crop = true );
		add_image_size( 'BlogPostImage', $width = 970, $height = 400, $crop = true );
		add_image_size( 'BlogPostCoverImage', $width = 300, $height = 200, $crop = true );
		add_image_size( 'ClientsSectionStyle1', $width = 1920, $height = 1443, $crop = true );
		add_image_size( 'ClientsSectionStyle2', $width = 1920, $height = 1084, $crop = true );
		add_image_size( 'ClientsSectionStyle3', $width = 1920, $height = 1253, $crop = true );
		add_image_size( 'ImageLogoHeder', $width = auto, $height = auto, $crop = true );



		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'master_wp_theme' ),
		) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'master_wp_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'master_wp_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function master_wp_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'master_wp_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'master_wp_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function master_wp_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'master_wp_theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'master_wp_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'master_wp_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function master_wp_theme_scripts() {
	/** Style Section**/

	wp_enqueue_style( 'master_wp_theme-style', get_stylesheet_uri() );
	wp_enqueue_style('master_wp_theme-libraries-bootstrap', get_template_directory_uri() . '/libraries/bootstrap/bootstrap.min.css');
	wp_enqueue_style('master_wp_theme-libraries-owl-carousel',  get_template_directory_uri() . '/libraries/owl-carousel/owl.carousel.css'); /** Core Owl Carousel CSS File  *	v1.3.3 **/
	wp_enqueue_style('master_wp_theme-libraries-owl.theme', get_template_directory_uri() . '/libraries/owl-carousel/owl.theme.css'); /** Core Owl Carousel CSS Theme  File  *	v1.3.3 --**/
	wp_enqueue_style('master_wp_theme-libraries-font-awesome', get_template_directory_uri() . '/libraries/fonts/font-awesome.min.css');
	wp_enqueue_style('master_wp_theme-libraries-elegant-icon', get_template_directory_uri() . '/libraries/fonts/elegant/elegant-icon.css');
	wp_enqueue_style('master_wp_theme-libraries-animate', get_template_directory_uri() . '/libraries/animate/animate.min.css');
	wp_enqueue_style('master_wp_theme-libraries-lightbox', get_template_directory_uri() . '/libraries/lightbox2/css/lightbox.css');
	wp_enqueue_style('master_wp_theme-libraries-YTPlayer', get_template_directory_uri() . '/libraries/video/YTPlayer.css');

	wp_enqueue_style('master_wp_theme-css-components', get_template_directory_uri() . '/css/components.css');
	wp_enqueue_style('master_wp_theme-css-header', get_template_directory_uri() . '/css/header.css');
	/**link id="color" href="css/color-schemes/default.css" rel="stylesheet"/**/
	wp_enqueue_style('master_wp_theme-css-media', get_template_directory_uri() . '/css/media.css');


	wp_enqueue_style('custom-google-font-Raleway', '//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900');
	wp_enqueue_style('custom-google-font-Montserrat', '//fonts.googleapis.com/css?family=Montserrat:400,700');
	wp_enqueue_style('custom-google-font-Roboto', '//fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic');

	/*************** Script Section************/
	/*Light Box*/
	/*Query Include*/
	wp_enqueue_script('libraries-jquery.min-js', get_theme_file_uri('/libraries/jquery.min.js'), NULL, '1.0', true);
	wp_enqueue_script('libraries-modernizr-custom-13711-js', get_theme_file_uri('/libraries/jquery.easing.min.js'), NULL, '1.0', true);
	wp_enqueue_script('libraries-jquery-easing-js', get_theme_file_uri('/libraries/jquery.easing.min.js'), NULL, '1.0', true);/*Easing Animation Effect*/
	wp_enqueue_script('libraries-bootstrap-min-js', get_theme_file_uri('/libraries/bootstrap/bootstrap.min.js'), NULL, '1.0', true); /*Core Bootstrap v3.2.0*/
	wp_enqueue_script('libraries-bootstrap-ie-emulation-modes-js', get_theme_file_uri('/libraries/bootstrap/ie-emulation-modes-warning.js'), NULL, '1.0', true); /*Just for debugging purposes. Don't actually copy these 2 lines!*/
	wp_enqueue_script('libraries-bootstrap-ie10-viewport-bug-workaround-js', get_theme_file_uri('/libraries/bootstrap/ie10-viewport-bug-workaround.js'), NULL, '1.0', true);/*E10 viewport hack for Surface/desktop Windows 8 bug*/ 
	/*Font Elegant*/
	/*[if lte IE 7]><script src="libraries/fonts/elegant/lte-ie7.js"></script><![endif]*/
	wp_enqueue_script('libraries-portfolio-filter-js', get_theme_file_uri('/libraries/portfolio-filter/jquery.quicksand.js'), NULL, '1.0', true); /*Quicksand v1.4*/
	wp_enqueue_script('libraries-jquery-superslides-js', get_theme_file_uri('/libraries/jquery.superslides.min.js'), NULL, '1.0', true); /*Superslides - v0.6.3-wip*/
	wp_enqueue_script('libraries-roundabout-js', get_theme_file_uri('/libraries/roundabout.js'), NULL, '1.0', true); /*service Rounded slider*/
	wp_enqueue_script('libraries-roundabout-shapes-js', get_theme_file_uri('/libraries/roundabout_shapes.js'), NULL, '1.0', true);/*service Rounded slider*/
	wp_enqueue_script('libraries-jquery-animateNumber-js', get_theme_file_uri('/libraries/jquery.animateNumber.min.js'), NULL, '1.0', true); /*Used for Animated Numbers*/
	wp_enqueue_script('libraries-jquery-appear-js', get_theme_file_uri('/libraries/jquery.appear.js'), NULL, '1.0', true);/*It Loads jQuery when element is appears*/
	wp_enqueue_script('libraries-jquery-knob-js', get_theme_file_uri('/libraries/jquery.knob.js'), NULL, '1.0', true);/*Used for Loading Circle*/
	wp_enqueue_script('libraries-wow-min-js', get_theme_file_uri('/libraries/wow.min.js'), NULL, '1.0', true);
	wp_enqueue_script('libraries-owl-carousel-js', get_theme_file_uri('/libraries/owl-carousel/owl.carousel.min.js'), NULL, '1.0', true); /*Core Owl Carousel CSS File  *	v1.3.3*/
	wp_enqueue_script('libraries-video-jquery-js', get_theme_file_uri('/libraries/video/jquery.mb.YTPlayer.js'), NULL, '1.0', true);
	wp_enqueue_script('libraries-lightbox2-js', get_theme_file_uri('/libraries/lightbox2/js/lightbox.min.js'), NULL, '1.0', true);
	
	/**Customized Scripts**/
	wp_enqueue_script('master_wp_theme-js-like', get_theme_file_uri('/js/modules/Like.js'),  array('jquery'), '1.0', true);
	wp_enqueue_script('master_wp_theme-js-api', get_theme_file_uri('/js/modules/GoogleMap.js'),  array('jquery'), '1.0', true);
	wp_enqueue_script('google_api_key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBh9b1rNCp6kOi5JeMHiRP4klDymBeoEWk', NULL, '1.0', true);
	wp_enqueue_script('master_wp_theme-js', get_theme_file_uri('/js/functions.js'), NULL, '1.0', true);
	wp_enqueue_script('master_wp_theme-js-soc', get_theme_file_uri('/js/modules/Social.js'),  array('jquery'), '1.0', true);
	wp_localize_script('master_wp_theme-js', 'masterWpThemeData', array(
    																	'root_url' => get_site_url(),
    																	'nonce' => wp_create_nonce('wp_rest')
  	));
	
	
	






	wp_enqueue_script( 'master_wp_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'master_wp_theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'master_wp_theme_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// function master_wp_theme_api() {
	
// 	acf_update_setting('google_api_key', 'AIzaSyBh9b1rNCp6kOi5JeMHiRP4klDymBeoEWk');
// }

// add_action('acf/init', 'master_wp_theme_api');

// Redirect subscriber accounts out of admin and onto homepage
add_action('admin_init', 'redirectSubsToFrontend');

function redirectSubsToFrontend() {
  $ourCurrentUser = wp_get_current_user();

  if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles[0] == 'subscriber') {
    wp_redirect(site_url('/'));
    exit;
  }
}

add_action('wp_loaded', 'noSubsAdminBar');

function noSubsAdminBar() {
  $ourCurrentUser = wp_get_current_user();

  if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles[0] == 'subscriber') {
    show_admin_bar(false);
  }
}

// Customize Login Screen
add_filter('login_headerurl', 'ourHeaderUrl');

function ourHeaderUrl() {
  return esc_url(site_url('/'));
}

add_action('login_enqueue_scripts', 'ourLoginCSS');

function ourLoginCSS() {
  wp_enqueue_style( 'master_wp_theme-style', get_stylesheet_uri() );
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
  wp_enqueue_style('custom-google-font-Raleway', '//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900');
	wp_enqueue_style('custom-google-font-Montserrat', '//fonts.googleapis.com/css?family=Montserrat:400,700');
	wp_enqueue_style('custom-google-font-Roboto', '//fonts.googleapis.com/css?family=Roboto:400,300italic,300,100italic,100,400italic,500,500italic,700,700italic,900,900italic');
}

add_filter('login_headertitle', 'ourLoginTitle');

function ourLoginTitle() {
  return get_bloginfo('name');
  }

		
//Theme Options

  if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
//Logo for Admin Panel

function my_login_logo_one() {
?>

<style type="text/css">
body.login div#login h1 a {
background-image: url(<?php $LogoImage = get_field('logo', 'option'); echo $LogoImage['sizes']['ImageLogoHeder']?>); 
padding-bottom: 30px;
}
</style>
<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo_one' );
