<?php get_header();?>
	<!-- LOADER -->
		<div id="site-loader" class="load-complete">
			<div class="load-position">
				<div class="logo"><img src="images/loading-logo.png" alt="loading-logo"/></div>
				<h6>Please wait, loading...</h6>
				<div class="loading">
					<div class="loading-line"></div>
					<div class="loading-break loading-dot-1"></div>
					<div class="loading-break loading-dot-2"></div>
					<div class="loading-break loading-dot-3"></div>
				</div>
			</div>
		</div>

		<!-- <div class="header-top" style="background-image: url(<?php echo get_theme_file_uri('/images/blog/header/top-header.jpg')?>);">
		<h1><?php the_title();?></h1>
		<p class="heading-description"><?php the_field('page_banner_subtitle')?></p>
		</div> -->
		<div class="page-banner container-fluid">
				<div class="header-top" style="background-image: url(<?php $PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['PageBannerBackgroundImage'];?>);"> 
				</div>
				<div class="page-banner__content heading header-top">
					<h1><?php the_title();?></h1>
			  		 <p class="heading-description"><?php the_field('page_banner_subtitle')?></p> 
				</div>
		</div>
	<?php while (have_posts()) {
		the_post(); ?>
		

		<div class="blog-page">
			<div class="container no-padding">
			<div class="blog-inner">
				<?php the_content();?>
			</div>
		</div>

		</div>


<?php }

get_footer(); 

?>
