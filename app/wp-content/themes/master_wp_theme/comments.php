<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Master_WP_Theme
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="add-comment">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<?php the_comments_navigation(); ?>

		<ul class="media-list">
			<h3>Comments</h3>
			<?php
			wp_list_comments( array(
				// 'style'      => 'li',
				// 'avatar_size'=> 60,
				// 'short_ping' => true,
				'callback' => 'custom_comments',
			) );
			?>
		</ul><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'master_wp_theme' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().
	?>

	<?php
		comment_form(array(
			'title_reply'	=>'Add a Comments',
			'label_submit'	=> 'Post',
			'comment_field' => '<div class="col-md-12 no-padding comment-form-comment">' . _x( '', 'noun' ) . '<input type="text" placeholder="Message" id="comment" name="comment" aria-required="true" /></div>',
			
			));?>
	

</div><!-- #comments -->
