
	<!-- Footer Section -->
	<?php 
					$args = array(
						'posts_per_pages' => '-1',
						'post_type'   => 'contact',
					);
				
				$frontpageContact = new WP_Query( $args);
				while ( $frontpageContact->have_posts()) {
					$frontpageContact->the_post('contact');?>
	<footer id="footer-section" class="footer-section">
		<div class="footer-item">
			<i class="icon_mail_alt"></i>
			<div class="footer-inner">
				<p class="footer-item-title">email</p>
				<a class="footer-item-desc"><?php the_field('contact_email')?></a>
			</div>
		</div>
		<div class="footer-item">
			<i class="icon_mobile"></i>
			<div class="footer-inner">
				<p class="footer-item-title">phone</p>
				<p class="footer-item-desc"><?php the_field('contact_phone')?></p>
			</div>
		</div>
		<a id="back-to-top" class="back-top pull-right"><i class="arrow_up"></i> Go on top</a>
	</footer>
	<?php } wp_reset_postdata();
			
	?>
	<!-- Footer Section -->
	
	<!-- Light Box -->
	<div class="modal light-box fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="false">
	<!-- Modal -->
		<div class="container">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close icon_close" data-dismiss="modal" aria-label="Close"></button>
							<div class="popup-heading">
								<p>Let’s take the next step togeder</p>
								<h1>start your project</h1>
							</div>
						</div>
						<div id="contact" class="modal-body">
							<div class="popup-form">
							<form action="<?php echo get_theme_file_uri('contact.php')?>" target="hidden" method="post">
								<div id="alert-msg" class="alert-msg"></div>
								<div class="form-group col-md-6 col-sm-6">
									<input type="text" class="form-control"  id="input_name" name="contact-name" placeholder="Name*" required="required"/>
								</div>
								<div class="form-group col-md-6 col-sm-6">
									<input type="text" class="form-control" id="input_compnay_name" name="contact-company-name" placeholder="Company"/>
								</div>
								<div class="form-group col-md-6 col-sm-6">
									<input type="email" class="form-control" id="input_email" name="contact-email" placeholder="Email*" required="required"/>
								</div>
								<div class="form-group col-md-6 col-sm-6">
									<input type="text" class="form-control" id="input_phone" name="contact-phone" placeholder="Telephone"/>
								</div>
								<div class="form-group col-md-6 col-sm-6">
									<input type="text" class="form-control" id="input_budget" name="contact-budget" placeholder="Budget*" required="required"/>
								</div>
								<div class="form-group col-md-6 col-sm-6">
									<input type="text" class="form-control" id="input_type" name="contact-type" placeholder="Type*" required="required"/>
								</div>
								<div class="form-group col-md-12 col-sm-12">
									<input type="text" class="form-control" id="textarea_message" name="contact-message" placeholder="Objectives"/>
								</div>
								<input type="submit" value="SEND" class="send" />
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	<!-- Light Box -->
	
	<!-- Hidden iframe for submitting forms -->
	<iframe src="about:blank" name="hidden" class="hide"></iframe>

<?php wp_footer(); ?>

</body>
</html>
