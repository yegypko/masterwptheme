<?php get_header();?>
		<!-- LOADER -->
		<!-- <div id="site-loader" class="load-complete">
			<div class="load-position">
				<div class="logo"><img src="images/loading-logo.png" alt="loading-logo"/></div>
				<h6>Please wait, loading...</h6>
				<div class="loading">
					<div class="loading-line"></div>
					<div class="loading-break loading-dot-1"></div>
					<div class="loading-break loading-dot-2"></div>
					<div class="loading-break loading-dot-3"></div>
				</div>
			</div>
		</div> -->
		<!-- <div class="container-fluid no-padding">
			<div class="row">
				<div class="header-top" style="background-image: url(<?php $PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['PageBannerBackgroundImage'];?>);"> 
					<h1><?php the_title();?></h1>
			  		<p class="heading-description"><?php the_field('page_banner_subtitle')?></p>
				</div>	
			</div>
		</div> -->
		<div class="page-banner container-fluid">

				<div class="header-top" style="background-image: url(<?php $PageBannerBackgroundImage = get_field('page_banner_background_image'); echo $PageBannerBackgroundImage['sizes']['PageBannerBackgroundImage'];?>);"> 

				</div>
				<div class="page-banner__content heading header-top">
					<h1><?php the_title();?></h1>
			  		 <p class="heading-description"><?php the_field('page_banner_subtitle')?></p> 
				</div>
	
				
			
			<!-- <img src="<?php echo get_theme_file_uri('/images/blog/header/top-header.jpg')?>"> -->

		</div>



		
		<?php while(have_posts()) {
				the_post(); ?>
		

		<div class="single-blog">
			<div class="container">

				

						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<div class="col-md-7 col-sm-6 no-l-padding by-admin">
								<p> By <?php the_author_posts_link();?> | <?php echo get_the_date('F j, Y');?> | <?php echo get_comments_number()?> Comments</p>
							</div>
							<?php

				              $likeCount = new WP_Query(array(
				                'post_type' => 'like',
				                'meta_query' => array(
				                  array(
				                    'key' => 'liked_post_id',
				                    'compare' => '=',
				                    'value' => get_the_ID()
				                  )
				                )
				              ));

				              $existStatus = 'no';

					              if (is_user_logged_in()) {
					                $existQuery = new WP_Query(array(
					                  'author' => get_current_user_id(),
					                  'post_type' => 'like',
					                  'meta_query' => array(
					                    array(
					                      'key' => 'liked_post_id',
					                      'compare' => '=',
					                      'value' => get_the_ID()
					                    )
					                  )
					                ));

					                if ($existQuery->found_posts) {
					                  $existStatus = 'yes';
					                }
					              }
				              ?>
							<div class="col-md-5 col-sm-6 no-r-padding single-social">
								<a href="#" class="like-box" data-like="<?php echo $existQuery->posts[0]->ID; ?>" data-post="<?php the_ID(); ?>" data-exists="<?php echo $existStatus; ?>">
									<i class="fa fa-heart" aria-hidden="true"></i> 
									<span class="like-count"><?php echo $likeCount->found_posts;?></span>
								</a>
								<!-- <a href="#"><i class="fa fa-facebook"></i> 95</a>
								<a href="#"><i class="fa fa-tumblr"></i> 195</a>
								<a href="#"><i class="fa fa-twitter"></i> 65</a> -->
							</div>
							 <?php the_content(); ?>
							<div class="entry-cover">
								<img src="<?php echo the_post_thumbnail_url('BlogPostImage'); ?>" alt="image 1">
							</div>
							<div class="entery-content">
								<p><?php the_content();?></p>
							</div>
							<div class="comment">
							<!-- If comments are open or we have at least one comment, load up the comment template. -->
								<ul class="media-list">
								<?php if ( comments_open() || get_comments_number() ) :
								     comments_template();
								 endif; ?>
								</ul>
							</div>
						</div>
						<div class="col-md-1">
						</div>

				

			</div>
		</div>
		


<?php } 

 get_footer();
  ?>